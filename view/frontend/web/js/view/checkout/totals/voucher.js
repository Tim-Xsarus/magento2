define(
    [
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/totals',
        'Magento_Checkout/js/model/quote'
    ],
    function (Component, totals, quote) {
        "use strict";

        return Component.extend({
            defaults: {
                displayZeroTotal: false,
                template: 'Spaaza_Loyalty/checkout/total'
            },
            getFloatValue: function () {
                var value = 0;
                var segment = totals.getSegment('spaaza_loyalty_voucher');
                if (segment) {
                    value = segment.value;
                }
                return value;
            },
            getTitle: function () {
                var title = '';
                var segment = totals.getSegment('spaaza_loyalty_voucher');
                if (segment) {
                    title = segment.title;
                }
                return title;
            },
            getValue: function () {
                return this.getFormattedPrice(this.getFloatValue());
            },
            isDisplayed: function () {
                var value = this.getFloatValue();
                return (value != 0 && value != null) || this.displayZeroTotal;
            }
        });
    }
);
