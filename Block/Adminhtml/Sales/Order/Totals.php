<?php

namespace Spaaza\Loyalty\Block\Adminhtml\Sales\Order;

/**
 * Class Totals
 *
 * @method \Magento\Sales\Block\Adminhtml\Order\Totals getParentBlock()
 */
class Totals extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $currency;

    /**
     * @var \Spaaza\Loyalty\Model\Order\SpaazaDataManagement
     */
    private $spaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    private $config;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Directory\Model\Currency $currency
     * @param \Spaaza\Loyalty\Model\Order\SpaazaDataManagement $spaazaDataManagement
     * @param \Spaaza\Loyalty\Model\Config $config
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Directory\Model\Currency $currency,
        \Spaaza\Loyalty\Model\Order\SpaazaDataManagement $spaazaDataManagement,
        \Spaaza\Loyalty\Model\Config $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->currency = $currency;
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->config = $config;
    }

    /**
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function getOrder()
    {
        return $this->getParentBlock()->getOrder();
    }

    /**
     * @return $this
     */
    public function initTotals()
    {
        $spaazaData = $this->spaazaDataManagement->applyExtensionAttributes($this->getOrder());

        if ($spaazaData->getVoucherAmount() != 0
            || $this->config->showVoucherZeroTotal($this->getOrder()->getStoreId())
        ) {
            $total = new \Magento\Framework\DataObject(
                [
                    'code' => 'spaaza_loyalty_vouchers',
                    'value' => -1 * $spaazaData->getVoucherAmount(),
                    'label' => __($this->config->getVoucherTotalLabel($this->getOrder()->getStoreId())),
                ]
            );
            $this->getParentBlock()->addTotalBefore($total, 'grand_total');
        }
        return $this;
    }
}
