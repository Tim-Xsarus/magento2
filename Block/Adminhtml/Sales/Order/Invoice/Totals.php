<?php

namespace Spaaza\Loyalty\Block\Adminhtml\Sales\Order\Invoice;

/**
 * Class Totals
 *
 * @method \Magento\Sales\Block\Adminhtml\Order\Invoice\Totals getParentBlock()
 */
class Totals extends \Magento\Sales\Block\Adminhtml\Order\Invoice\Totals
{
    /**
     * @var \Spaaza\Loyalty\Model\Invoice\SpaazaDataManagement
     */
    private $spaazaDataManagement;
    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    private $config;

    /**
     * Totals constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Helper\Admin $adminHelper
     * @param \Spaaza\Loyalty\Model\Invoice\SpaazaDataManagement $spaazaDataManagement
     * @param \Spaaza\Loyalty\Model\Config $config
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        \Spaaza\Loyalty\Model\Invoice\SpaazaDataManagement $spaazaDataManagement,
        \Spaaza\Loyalty\Model\Config $config,
        array $data = []
    ) {
        parent::__construct($context, $registry, $adminHelper, $data);
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->config = $config;
    }

    public function initTotals()
    {
        $invoice = $this->getInvoice();

        $spaazaData = $this->spaazaDataManagement->applyExtensionAttributes($invoice);
        if ($spaazaData->getVoucherAmount() > 0 || $this->config->showVoucherZeroTotal($invoice->getStoreId())) {
            $total = new \Magento\Framework\DataObject(
                [
                    'code' => 'spaaza_loyalty_vouchers',
                    'value' => -1 * $spaazaData->getVoucherAmount(),
                    'base_value' => -1 * $spaazaData->getBaseVoucherAmount(),
                    'label' => __($this->config->getVoucherTotalLabel($invoice->getStoreId()))
                ]
            );
            $this->getParentBlock()->addTotalBefore($total, 'grand_total');
        }
        return $this;
    }
}
