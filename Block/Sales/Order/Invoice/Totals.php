<?php

namespace Spaaza\Loyalty\Block\Sales\Order\Invoice;

/**
 * Class Totals
 *
 * @method \Magento\Sales\Block\Order\Invoice\Totals getParentBlock()
 */
class Totals extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \Spaaza\Loyalty\Model\Invoice\SpaazaDataManagement
     */
    private $spaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    private $config;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Spaaza\Loyalty\Model\Invoice\SpaazaDataManagement $spaazaDataManagement,
        \Spaaza\Loyalty\Model\Config $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->config = $config;
    }

    /**
     * @return \Magento\Sales\Api\Data\InvoiceInterface
     */
    public function getInvoice()
    {
        return $this->getParentBlock()->getInvoice();
    }

    /**
     * @return $this
     */
    public function initTotals()
    {
        $spaazaData = $this->spaazaDataManagement->applyExtensionAttributes($this->getInvoice());

        if ($spaazaData->getVoucherAmount() != 0
            || $this->config->showVoucherZeroTotal($this->getInvoice()->getStoreId())
        ) {
            $total = new \Magento\Framework\DataObject(
                [
                    'code' => 'spaaza_loyalty_vouchers',
                    'value' => -1 * $spaazaData->getVoucherAmount(),
                    'base_value' => -1 * $spaazaData->getBaseVoucherAmount(),
                    'label' => __($this->config->getVoucherTotalLabel($this->getInvoice()->getStoreId())),
                ]
            );
            $this->getParentBlock()->addTotalBefore($total, 'grand_total');
        }
        return $this;
    }
}
