REST API
========

This module exposes several functions through the Magento REST API.

Customer Authentication
-----------------------

**POST** `/rest/V1/spaaza/customer/authenticate`

| Parameter      | Description |
|----------------|-------------|
| `email`        | The email address of the customer to test authentication for |
| `password`     | The password to check |

### Example
```json
{
    "email": "info@spaaza.com",
    "password": "ThisIsEasyToGuess"
}
```

### Result

**Unsuccessful**
```json
{
    "authenticated": false,
    "customer_id": null,
    "message": "Invalid login or password."
}
```

**Successful**
```json
{
    "authenticated": true,
    "customer_id": 7,
    "message": null
}
```

Customer Update
-----------------------

This endpoint can be used to create or update a Magento customer. It receives Spaaza user info and processes it in the same way as the regular data pull does (after logging in). It returns a Magento customer object. 

**POST** `/rest/V1/spaaza/customer/update`

| Parameter              | Type     | Description      |
|------------------------|----------|------------------|
| `user_info`            | _object_ | The user data as it would have been returned by the Spaaza `get-user` endpoint. The keys `user_id`, `member_number`, `username` and `authentication_point_identifier` are used in this specific order to identify an existing customer.  |
| `create_if_not_exists` | _bool_   | If `true`, create a new Magento customer if no customer can be identified using the provided data. Default: false. |

### Identification
The key `user_id` will, if supplied, be used to identify a customer. You can then also update other Spaaza specific data like `member_number`. If `user_id` does not exist (or is considered empty by PHP) in `user_info`, `member_number` will be used to identify the customer. If using one of these two fields does not find a customer, `username` is tried and then as a last method `authentication_point_identifier`  will be used.

If `username` or `authentication_point_identifier` has been used to identify the customer, `user_id` and `member_number` will be set and saved on the found customer.

### Example
```json
{
    "user_info": {
        "member_number": "99102352",
        "user_id": 3362356,
        "username": "info@spaaza.com",
        
        "password": "EasyOrNot?",

        "address_streetname": "Dorpstraat",
        "address_housenumber": "1",
        "address_housenumber_extension": null,
        "address_line_2": null,
        "address_line_3": null,
        "address_postalcode": "1000AA",
        "address_regionstate": null,
        "address_towncity": "Amsterdam",
        "birthday": "2002-01-01T00:00:00+00:00",
        "country_code": "NL",
        "entity_code": {
            "code": "99102352",
            "type": "custom"
        },
        "first_name": "John",
        "last_name": "Doe",
        "gender": "F",
        "id": 3362356,
        "mailing_list": {
            "mailing_list_sub_offered": true,
            "mailing_list_subscribed": true,
            "printed_mailing_list_subscribed": false
        },
        "opt_in_programme": {
            "programme_opted_in": false
        },
        "phone_number": "0851234567"
    },
    "create_if_not_exists": true
}
```

### Result

**Unsuccessful** _(HTTP status 404)_
```json
{
    "message": "Customer not found"
}
```

**Successful**
```json
{
    "id": 39,
    "group_id": 1,
    "default_billing": "8",
    "default_shipping": "8",
    "created_at": "2018-10-18 14:49:13",
    "updated_at": "2018-10-18 21:06:56",
    "created_in": "Default Store View",
    "dob": "2002-01-01",
    "email": "info@spaaza.com",
    "firstname": "John",
    "lastname": "Doe",
    "gender": 2,
    "store_id": 1,
    "website_id": 1,
    "addresses": [
        {
            "id": 8,
            "customer_id": 39,
            "region": {
                "region_code": null,
                "region": null,
                "region_id": 0
            },
            "region_id": 0,
            "country_id": "NL",
            "street": [
                "Dorpstraat",
                "1"
            ],
            "telephone": "0851234567",
            "postcode": "1000AA",
            "city": "Amsterdam",
            "firstname": "John",
            "lastname": "Doe",
            "default_shipping": true,
            "default_billing": true
        }
    ],
    "disable_auto_group_change": 0,
    "extension_attributes": {
        "is_subscribed": false,
        "spaaza_data": {
            "customer_id": 39,
            "user_id": 3362356,
            "member_number": "99102352",
            "programme_opted_in": false,
            "mailing_list_subscribed": true,
            "printed_mailing_list_subscribed": false,
            "last_hash": "deadf385f4407f78"
        }
    }
}
```

Customer Info By Id
-----------------------

**GET** `/rest/V1/spaaza/customer/user_info/:customerId`

Gets the user info for a customer as it would be sent to the Spaaza API.

### Example
```
GET /rest/V1/spaaza/customer/user_info/1
```

### Result
```json
{
    "first_name": "Veronica",
    "last_name": "Costello",
    "birthday": "1973-12-15",
    "username": "roni_cost@example.com",
    "webshop_customer_id": 1,
    "gender": "F",
    "printed_mailing_list_subscribed": false,
    "mailing_list_subscribed": false,
    "mailing_list_sub_offered": true,
    "programme_opted_in": false,
    "address_streetname": "Test Street",
    "address_housenumber": "8",
    "address_housenumber_extension": "",
    "address_postalcode": "49628-7978",
    "address_towncity": "Amsterdam",
    "country_code": "NL",
    "phone_number": "010 123 45 67"
}
```

Customer Info By Email
-----------------------

**GET** `/rest/V1/spaaza/customer/user_info_by_email/:customerEmail`

Gets the user info for a customer as it would be sent to the Spaaza API.

### Example
```
GET /rest/V1/spaaza/customer/user_info/roni_cost@example.com
```

### Result
```json
{
    "first_name": "Veronica",
    "last_name": "Costello",
    "birthday": "1973-12-15",
    "username": "roni_cost@example.com",
    "webshop_customer_id": 1,
    "gender": "F",
    "printed_mailing_list_subscribed": false,
    "mailing_list_subscribed": false,
    "mailing_list_sub_offered": true,
    "programme_opted_in": false,
    "address_streetname": "Test Street",
    "address_housenumber": "8",
    "address_housenumber_extension": "",
    "address_postalcode": "49628-7978",
    "address_towncity": "Amsterdam",
    "country_code": "NL",
    "phone_number": "010 123 45 67"
}
```