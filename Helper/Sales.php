<?php

namespace Spaaza\Loyalty\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Sales\Api\Data\CreditmemoInterface;
use Magento\Sales\Api\Data\OrderInterface;

class Sales extends AbstractHelper
{

    /**
     * Get the owner code for an order
     *
     * @param OrderInterface $order
     * @return string
     */
    public function getOrderOwnerCode(OrderInterface $order)
    {
        return $order->getIncrementId();
    }

    /**
     * Get the owner code for a credit memo; distinct it from orders
     *
     * @param CreditmemoInterface $creditmemo
     * @return string
     */
    public function getCreditmemoOwnerCode(CreditmemoInterface $creditmemo)
    {
        return 'C' . $creditmemo->getIncrementId();
    }
}
