<?php

namespace Spaaza\Loyalty\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Quote\Api\Data\CartInterface;

class Quote extends AbstractHelper
{

    /**
     * Compose the voucher locking code for a quote
     *
     * @param CartInterface $quote
     * @return string|null
     */
    public function getVoucherLockingCode(CartInterface $quote)
    {
        if ($quote->getId()) {
            return 'MQ' . $quote->getId();
        }
        return null;
    }
}
