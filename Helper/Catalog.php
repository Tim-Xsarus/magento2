<?php

namespace Spaaza\Loyalty\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Sales\Api\Data\CreditmemoItemInterface;
use Magento\Sales\Api\Data\OrderItemInterface;

class Catalog extends AbstractHelper
{

    /**
     * Get the identification of a product to send to Spaaza
     *
     * For now, sku is the only attribute that can be used. Of course, feel free to create
     * a plugin for this method!
     *
     * See the documentation of Spaaza:
     * https://docs.spaaza.com/#adding-a-completed-basket
     *
     * @param OrderItemInterface $orderItem
     * @return array
     */
    public function getBasketItemIdentification(OrderItemInterface $orderItem)
    {
        return ['item_barcode' => $orderItem->getSku()];
    }

    /**
     *
     * @see getBasketItemIdentification()
     * @param CreditmemoItemInterface $creditmemoItem
     * @return array
     */
    public function getBasketItemIdentificationForCreditmemoItem(CreditmemoItemInterface $creditmemoItem)
    {
        return ['item_barcode' => $creditmemoItem->getSku()];
    }
}
