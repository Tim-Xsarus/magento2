<?php

namespace Spaaza\Loyalty\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Notification\MessageInterface;

class Data extends AbstractHelper
{
    const TOTAL_CODE = 'spaaza_loyalty_voucher';

    /**
     * @var \Magento\Framework\Module\ModuleListInterface
     */
    protected $moduleList;

    /**
     * @var \Magento\Framework\Notification\NotifierPool
     */
    protected $notifierPool;

    /**
     * @var \Spaaza\Loyalty\Model\Config\Source\EntityType
     */
    protected $entityTypeSource;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    protected $config;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Spaaza\Loyalty\Model\Config\Source\EntityType $entityTypeSource,
        \Magento\Framework\Notification\NotifierPool $notifierPool,
        \Spaaza\Loyalty\Model\Config $config,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->moduleList = $moduleList;
        $this->notifierPool = $notifierPool;
        $this->entityTypeSource = $entityTypeSource;
        $this->logger = $logger;
        $this->config = $config;
        parent::__construct($context);
    }

    /**
     * Get the Magento module version
     *
     * @return string
     */
    public function getModuleVersion()
    {
        return (string)$this->moduleList->getOne('Spaaza_Loyalty')['setup_version'];
    }

    /**
     * Get the label for an entity type
     *
     * @param $type
     * @return string
     */
    public function getEntityTypeLabel($type)
    {
        return $this->entityTypeSource->getLabelForValue($type);
    }

    /**
     * Add a line to the debug log
     *
     * @param mixed $message  Logs this message; logs an error if this is an Exception object
     * @param array $context
     * @return void
     */
    public function debugLog($message, $context = [])
    {
        if ($this->config->isDebugLogEnabled()) {
            if ($message instanceof \Exception) {
                $context = ['type' => get_class($message)];
                $this->logger->error($message->getMessage(), $context);
            } else {
                $this->logger->info($message, $context);
            }
        }
    }

    // TODO: publish the error
    // phpcs:ignore
    public function publishError($type, $title, $message, $severity = MessageInterface::SEVERITY_MINOR, $context = [])
    {
    }

    /**
     * Get the sort order to use for displaying the vouchers total
     *
     * @return int
     */
    public function getVoucherTotalSortOrder()
    {
        return $this->config->getVoucherTotalSortOrder();
    }

    /**
     * Get the label to use for the vouchers total
     *
     * @return string
     */
    public function getVoucherTotalLabel()
    {
        return $this->config->getVoucherTotalLabel();
    }
}
