<?php

namespace Spaaza\Loyalty\Model\QuoteAddress;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\Data\AddressExtensionInterface;
use Magento\Quote\Api\Data\AddressInterface;
use Spaaza\Loyalty\Api\Data\QuoteAddress\SpaazaDataInterface;

class SpaazaDataManagement
{

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    private $helper;

    /**
     * @var \Spaaza\Loyalty\Api\Data\QuoteAddress\SpaazaDataInterfaceFactory
     */
    private $spaazaDataFactory;

    /**
     * @var \Magento\Quote\Api\Data\AddressExtensionInterfaceFactory
     */
    private $quoteAddressExtensionFactory;

    /**
     * @var \Spaaza\Loyalty\Model\ResourceModel\QuoteAddress\SpaazaData
     */
    private $spaazaDataResourceModel;

    /**
     * SpaazaDataManagement constructor.
     *
     * @param \Spaaza\Loyalty\Helper\Data $helper
     * @param \Spaaza\Loyalty\Api\Data\QuoteAddress\SpaazaDataInterfaceFactory $spaazaDataFactory
     * @param \Magento\Quote\Api\Data\AddressExtensionInterfaceFactory $quoteAddressExtensionFactory
     * @param \Spaaza\Loyalty\Model\ResourceModel\QuoteAddress\SpaazaData $spaazaDataResourceModel
     */
    public function __construct(
        \Spaaza\Loyalty\Helper\Data $helper,
        \Spaaza\Loyalty\Api\Data\QuoteAddress\SpaazaDataInterfaceFactory $spaazaDataFactory,
        \Magento\Quote\Api\Data\AddressExtensionInterfaceFactory $quoteAddressExtensionFactory,
        \Spaaza\Loyalty\Model\ResourceModel\QuoteAddress\SpaazaData $spaazaDataResourceModel
    ) {
        $this->helper = $helper;
        $this->spaazaDataFactory = $spaazaDataFactory;
        $this->quoteAddressExtensionFactory = $quoteAddressExtensionFactory;
        $this->spaazaDataResourceModel = $spaazaDataResourceModel;
    }

    /**
     * Ensures existing extension attributes and (loaded) Spaaza data
     *
     * @param AddressInterface $address
     * @return SpaazaDataInterface
     */
    public function applyExtensionAttributes(AddressInterface $address)
    {
        $extensionAttributes = $address->getExtensionAttributes();
        if (!$extensionAttributes) {
            $extensionAttributes = $this->quoteAddressExtensionFactory->create();
            $this->helper->debugLog(
                'Added extension attributes object to quote address',
                ['quote_address' => $address->getId() ?: 'new']
            );
        }

        $spaazaData = $extensionAttributes->getSpaazaData();
        if (!$spaazaData) {
            if ($address->getId()) {
                try {
                    $spaazaData = $this->getByQuoteAddressId($address->getId());
                    $this->helper->debugLog(
                        'Got extension attribute from database',
                        ['quote_address' => $address->getId()]
                    );
                } catch (NoSuchEntityException $e) {
                    $spaazaData = $this->spaazaDataFactory->create();
                    $spaazaData->setQuoteAddressId($address->getId());
                    $this->helper->debugLog(
                        'Created a new extension attribute for existing quote address',
                        ['address' => $address->getId()]
                    );
                }
            } else {
                $spaazaData = $this->spaazaDataFactory->create();
                $this->helper->debugLog('Created a new extension attribute for new quote address');
            }
        }

        $extensionAttributes->setSpaazaData($spaazaData);
        $address->setExtensionAttributes($extensionAttributes);

        return $spaazaData;
    }

    /**
     * Retrieve Quote Address SpaazaData By Quote Address Id
     *
     * @param int $addressId
     * @return SpaazaDataInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByQuoteAddressId($addressId)
    {
        $spaazaData = $this->spaazaDataFactory->create();
        $this->spaazaDataResourceModel->load($spaazaData, $addressId);
        if (!$spaazaData->getQuoteAddressId()) {
            throw new NoSuchEntityException(__('SpaazaData for quote address id "%1" does not exist.', $addressId));
        }
        return $spaazaData;
    }

    /**
     * Save Order SpaazaData
     *
     * @param SpaazaDataInterface $data
     * @return SpaazaDataInterface
     * @throws CouldNotSaveException
     */
    public function save(SpaazaDataInterface $data)
    {
        if (!$data->getQuoteAddressId()) {
            throw new CouldNotSaveException(__('Cannot save quote address Spaaza data without an address id'));
        }
        try {
            $this->helper->debugLog(
                'Save extension attribute',
                ['quote_address' => $data->getQuoteAddressId()]
            );
            $this->spaazaDataResourceModel->save($data);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__('Could not save quote address Spaaza data: %1', $exception->getMessage()));
        }
        return $data;
    }
}
