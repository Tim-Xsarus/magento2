<?php

namespace Spaaza\Loyalty\Model\Total\Invoice;

class Voucher extends \Magento\Sales\Model\Order\Invoice\Total\AbstractTotal
{
    /**
     * @var \Spaaza\Loyalty\Model\Invoice\SpaazaDataManagement
     */
    private $invoiceSpaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Model\Order\SpaazaDataManagement
     */
    private $orderSpaazaDataManagement;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    public function __construct(
        \Spaaza\Loyalty\Model\Invoice\SpaazaDataManagement $invoiceSpaazaDataManagement,
        \Spaaza\Loyalty\Model\Order\SpaazaDataManagement $orderSpaazaDataManagement,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        array $data = []
    ) {
        $this->invoiceSpaazaDataManagement = $invoiceSpaazaDataManagement;
        $this->orderSpaazaDataManagement = $orderSpaazaDataManagement;
        $this->orderRepository = $orderRepository;
        parent::__construct($data);
    }

    /**
     * Collect invoice Voucher total
     *
     * @param \Magento\Sales\Model\Order\Invoice $invoice
     * @return \Magento\Sales\Model\Order\Invoice\Total\AbstractTotal
     */
    public function collect(\Magento\Sales\Model\Order\Invoice $invoice)
    {
        $order = $this->orderRepository->get($invoice->getOrderId());

        $invoiceSpaazaData = $this->invoiceSpaazaDataManagement->applyExtensionAttributes($invoice);
        $orderSpaazaData = $this->orderSpaazaDataManagement->applyExtensionAttributes($order);

        if ($orderSpaazaData->getVoucherAmount() > 0
            && $orderSpaazaData->getVoucherAmountInvoiced() != $orderSpaazaData->getVoucherAmount()
        ) {
            // Determine the amount left to invoice
            $voucherAmountLeft = $orderSpaazaData->getVoucherAmount() - $orderSpaazaData->getVoucherAmountInvoiced();
            $baseVoucherAmountLeft = $orderSpaazaData->getBaseVoucherAmount()
                - $orderSpaazaData->getBaseVoucherAmountInvoiced();

            // Determine the amount to use: it can be no more than the invoice grand total
            $voucherAmountUsed = min($voucherAmountLeft, $invoice->getGrandTotal());
            $baseVoucherAmountUsed = min($baseVoucherAmountLeft, $invoice->getBaseGrandTotal());

            // Set the used amount on the extension attribute
            $invoiceSpaazaData->setVoucherAmount($voucherAmountUsed);
            $invoiceSpaazaData->setBaseVoucherAmount($baseVoucherAmountUsed);

            // Decrease the invoice's grand total
            $invoice->setGrandTotal(max($invoice->getGrandTotal() - $voucherAmountUsed, 0));
            $invoice->setBaseGrandTotal(max($invoice->getBaseGrandTotal() - $baseVoucherAmountUsed, 0));
        }
        return parent::collect($invoice);
    }
}
