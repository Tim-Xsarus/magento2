<?php

namespace Spaaza\Loyalty\Model\Total\Quote;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address\Total as AddressTotal;

class Voucher extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{
    /**
     * @var \Spaaza\Loyalty\Api\Data\VoucherInfoInterfaceFactory
     */
    protected $voucherInfoFactory;

    /**
     * @var \Spaaza\Loyalty\Model\QuoteAddress\SpaazaDataManagement
     */
    private $spaazaDataManagement;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    private $helper;

    /**
     * @var \Spaaza\Loyalty\Model\VoucherManagement
     */
    private $voucherManagement;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    public function __construct(
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Spaaza\Loyalty\Model\QuoteAddress\SpaazaDataManagement $spaazaDataManagement,
        \Spaaza\Loyalty\Helper\Data $helper,
        \Spaaza\Loyalty\Model\VoucherManagement $voucherManagement,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Spaaza\Loyalty\Api\Data\VoucherInfoInterfaceFactory $voucherInfoFactory
    ) {
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->priceCurrency = $priceCurrency;
        $this->helper = $helper;
        $this->voucherManagement = $voucherManagement;
        $this->customerRepository = $customerRepository;
        $this->voucherInfoFactory = $voucherInfoFactory;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->helper->getVoucherTotalLabel();
    }

    /**
     * @api
     * @param Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param AddressTotal $total
     * @return AddressTotal\AbstractTotal
     */
    public function collect(Quote $quote, ShippingAssignmentInterface $shippingAssignment, AddressTotal $total)
    {
        parent::collect($quote, $shippingAssignment, $total);

        if (!$quote->getCustomerId()) {
            return $this;
        }

        $address = $shippingAssignment->getShipping()->getAddress();
        $spaazaData = $this->spaazaDataManagement->applyExtensionAttributes($address);

        if (empty($address->getAllItems())) {
            return $this;
        }

        try {
            $customer = $this->customerRepository->getById($quote->getCustomerId());
        } catch (NoSuchEntityException $e) {
            return $this;
        }

        $totalUsedVoucherAmount = 0;

        $availableVouchers = $this->voucherManagement->getAvailableVouchers($customer);
        $voucherInfos = [];
        foreach ($availableVouchers as $voucher) {
            if ($this->voucherManagement->canSpend($voucher, $quote)) {
                /** @var \Spaaza\Loyalty\Api\Data\VoucherInfoInterface $voucherData */
                $voucherData = $this->voucherInfoFactory->create([
                    'data' => [
                        'label' => $voucher->getVoucherText(),
                        'type' => $voucher->getVoucherType(),
                        'amount' => round($voucher->getVoucherAmount(), 2),
                        'key' => $voucher->getVoucherKey()
                    ]
                ]);
                $voucherInfos[] = $voucherData;
                $totalUsedVoucherAmount += $voucher->getVoucherAmount();
            }
        }
        $spaazaData->setVouchers($voucherInfos);

        if ($customer->getExtensionAttributes()
            && $customerSpaazaData = $customer->getExtensionAttributes()->getSpaazaData()
        ) {
            $spaazaData->setUserId($customerSpaazaData->getUserId());
            $spaazaData->setMemberNumber($customerSpaazaData->getMemberNumber());
        }

        $shippingAmount = $total->getShippingInclTax();
        $totalUsedVoucherAmount = min($totalUsedVoucherAmount, $total->getGrandTotal() - $shippingAmount);

        // @todo Convert to base amount
        $baseTotalUsedVoucherAmount = $totalUsedVoucherAmount;

        $spaazaData->setVoucherAmount($totalUsedVoucherAmount);
        $spaazaData->setBaseVoucherAmount($baseTotalUsedVoucherAmount);

        $total->setTotalAmount(
            \Spaaza\Loyalty\Helper\Data::TOTAL_CODE,
            -1 * round($spaazaData->getVoucherAmount(), 2)
        );

        $total->setBaseTotalAmount(
            \Spaaza\Loyalty\Helper\Data::TOTAL_CODE,
            -1 * round($spaazaData->getBaseVoucherAmount(), 2)
        );

        $total->setGrandTotal($total->getGrandTotal() - $totalUsedVoucherAmount);
        $total->setBaseGrandTotal($total->getBaseGrandTotal() - $baseTotalUsedVoucherAmount);

        if ($address->getId()) {
            $spaazaData->setQuoteAddressId($address->getId());
            $this->spaazaDataManagement->save($spaazaData);
        }

        return $this;
    }

    /**
     * @param Quote $quote
     * @param AddressTotal $total
     * @return array
     */
    public function fetch(Quote $quote, AddressTotal $total)
    {
        $value = 0;
        foreach ($quote->getAllAddresses() as $address) {
            $this->spaazaDataManagement->applyExtensionAttributes($address);
            $extensionAttributes = $address->getExtensionAttributes();
            if ($extensionAttributes && $extensionAttributes->getSpaazaData()) {
                $value += $extensionAttributes->getSpaazaData()->getVoucherAmount();
            }
        }

        return [
            'code' => \Spaaza\Loyalty\Helper\Data::TOTAL_CODE,
            'title' => __($this->getLabel()),
            'value' => -1 * $value,
            'full_info' => [],
        ];
    }
}
