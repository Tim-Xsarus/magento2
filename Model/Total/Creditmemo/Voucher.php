<?php

namespace Spaaza\Loyalty\Model\Total\Creditmemo;

use Magento\Sales\Api\Data\CreditmemoItemInterface;

class Voucher extends \Magento\Sales\Model\Order\Creditmemo\Total\AbstractTotal
{
    /**
     * @var \Spaaza\Loyalty\Model\Order\SpaazaDataManagement
     */
    private $orderSpaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Model\Creditmemo\SpaazaDataManagement
     */
    private $creditmemoSpaazaDataManagement;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var \Spaaza\Loyalty\Helper\Catalog
     */
    private $spaazaCatalogHelper;

    /**
     * Voucher total constructor.
     *
     * @param \Spaaza\Loyalty\Model\Creditmemo\SpaazaDataManagement $creditmemoSpaazaDataManagement
     * @param \Spaaza\Loyalty\Model\Order\SpaazaDataManagement $orderSpaazaDataManagement
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Spaaza\Loyalty\Helper\Catalog $spaazaCatalogHelper
     * @param array $data
     */
    public function __construct(
        \Spaaza\Loyalty\Model\Creditmemo\SpaazaDataManagement $creditmemoSpaazaDataManagement,
        \Spaaza\Loyalty\Model\Order\SpaazaDataManagement $orderSpaazaDataManagement,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Spaaza\Loyalty\Helper\Catalog $spaazaCatalogHelper,
        array $data = []
    ) {
        $this->orderSpaazaDataManagement = $orderSpaazaDataManagement;
        $this->creditmemoSpaazaDataManagement = $creditmemoSpaazaDataManagement;
        $this->orderRepository = $orderRepository;
        $this->spaazaCatalogHelper = $spaazaCatalogHelper;
        parent::__construct($data);
    }

    /**
     * Collect invoice Voucher total
     *
     * @param \Magento\Sales\Model\Order\Creditmemo $creditmemo
     * @return $this
     */
    public function collect(\Magento\Sales\Model\Order\Creditmemo $creditmemo)
    {
        $order = $this->orderRepository->get($creditmemo->getOrderId());

        $creditmemoSpaazaData = $this->creditmemoSpaazaDataManagement->applyExtensionAttributes($creditmemo);
        $orderSpaazaData = $this->orderSpaazaDataManagement->applyExtensionAttributes($order);

        if ($orderSpaazaData->getVoucherAmount() > 0 && is_array($orderSpaazaData->getVoucherDistribution())) {
            $voucherDistributon = $orderSpaazaData->getVoucherDistribution();
            $voucherAmount = 0;
            foreach ($creditmemo->getAllItems() as $creditmemoItem) {
                $amountPerUnit = $this->getVoucherAmountPerUnit($creditmemoItem, $voucherDistributon);
                $voucherAmount += $amountPerUnit * $creditmemoItem->getQty();
            }

            // @todo Convert to base amount
            $baseVoucherAmount = $voucherAmount;

            // Set the used amount on the extension attribute
            $creditmemoSpaazaData->setVoucherAmount($voucherAmount);
            $creditmemoSpaazaData->setBaseVoucherAmount($baseVoucherAmount);

            // Decrease the invoice's grand total
            $creditmemo->setGrandTotal(max($creditmemo->getGrandTotal() - $voucherAmount, 0));
            $creditmemo->setBaseGrandTotal(max($creditmemo->getBaseGrandTotal() - $baseVoucherAmount, 0));
        }
        return parent::collect($creditmemo);
    }

    /**
     * Return the applied voucher amount per product, as it was saved in the order extension attribute
     *
     * @param CreditmemoItemInterface $creditmemoItem
     * @param $voucherDistribution
     * @return float
     */
    private function getVoucherAmountPerUnit(CreditmemoItemInterface $creditmemoItem, $voucherDistribution)
    {
        $identification = $this->spaazaCatalogHelper->getBasketItemIdentificationForCreditmemoItem($creditmemoItem);
        if ($identification && !empty($identification['item_barcode'])) {
            foreach ($voucherDistribution as $voucherInfo) {
                if ($identification['item_barcode'] == $voucherInfo['item_barcode']) {
                    if (!empty($voucherInfo['amount_total'])) {
                        if (!isset($voucherInfo['qty'])) {
                            $qty = 1;
                        } else {
                            $qty = (float)$voucherInfo['qty'];
                        }
                        if ($qty > 0) {
                            return (float)($voucherInfo['amount_total'] / $qty);
                        }
                    }
                }
            }
        }
        return 0;
    }
}
