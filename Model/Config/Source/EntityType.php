<?php

namespace Spaaza\Loyalty\Model\Config\Source;

use Magento\Framework\View\Element\AbstractBlock;

class EntityType implements \Magento\Framework\Data\OptionSourceInterface
{
    const ENTITY_TYPE_ORDER = 'order';
    const ENTITY_TYPE_CREDITMEMO = 'creditmemo';
    const ENTITY_TYPE_CUSTOMER = 'customer';

    /**
     * @inheritdoc
     * @return array
     */
    public function toOptionArray()
    {
        $result = [];
        foreach ($this->getAllOptions() as $option) {
            $result[] = [
                'label' => $option['label'],
                'value' => $option['value'],
            ];
        }
        return $result;
    }

    /**
     * Get all options with some 'special keys'
     *
     * @return array  Array of associative arrays with option info
     */
    public function getAllOptions()
    {
        return [
            [
                'value' => self::ENTITY_TYPE_ORDER,
                'label' => _('Order'),
                'adminhtml_link' => ['sales/order/view', ['order_id' => '{{id}}']],
                'send_link' => ['spaaza/queue/sendOrder', ['order_id' => '{{id}}']],
            ],
            [
                'value' => self::ENTITY_TYPE_CREDITMEMO,
                'label' => _('Credit Memo'),
                'adminhtml_link' => ['sales/order_creditmemo/view', ['creditmemo_id' => '{{id}}']],
                'send_link' => ['spaaza/queue/sendCreditmemo', ['creditmemo_id' => '{{id}}']],
            ],
            [
                'value' => self::ENTITY_TYPE_CUSTOMER,
                'label' => _('Customer'),
                'adminhtml_link' => ['customer/index/edit', ['id' => '{{id}}']],
                'send_link' => ['spaaza/queue/sendCustomer', ['customer_id' => '{{id}}']],
            ],
        ];
    }

    /**
     * Get the backend-link to an item
     *
     * @param string $entityType
     * @param int $id
     * @param AbstractBlock $block  The block to use for URL generation
     * @return string|null  Or NULL if the type is not found
     */
    public function getAdminhtmlUrl($entityType, $id, AbstractBlock $block)
    {
        return $this->_getActionUrl('adminhtml_link', $entityType, $id, $block);
    }

    /**
     * Get the link to send an item to Spaaza
     *
     * @param string $entityType
     * @param int $id
     * @param AbstractBlock $block  The block to use for URL generation
     * @return string|null Or NULL if the type is not found
     */
    public function getSendUrl($entityType, $id, AbstractBlock $block)
    {
        return $this->_getActionUrl('send_link', $entityType, $id, $block);
    }

    /**
     * @see getAllOptions()
     * @param string $action  Action like defined in the option
     * @param string $entityType  One of the ENTITY_TYPE_XX constants
     * @param int $id
     * @param AbstractBlock $block  The block to use for URL generation
     * @return null|string
     */
    protected function _getActionUrl($action, $entityType, $id, AbstractBlock $block)
    {
        $option = $this->getOptionByValue($entityType);
        if ($option && !empty($option[$action])) {
            $link = $option[$action];
            $url = $block->getUrl($link[0], !empty($link[1]) ? $link[1] : []);
            return strtr($url, [
                '{{id}}' => (int)$id,
            ]);
        }
        return null;
    }

    /**
     * Get the label for a value from the option list
     *
     * @param string $value
     * @return string
     */
    public function getLabelForValue($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if (isset($option['value']) && $option['value'] == $value && isset($option['label'])) {
                return $option['label'];
            }
        }
        return $value;
    }
}
