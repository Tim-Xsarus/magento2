<?php

namespace Spaaza\Loyalty\Model\ResourceModel\QuoteAddress;

class SpaazaData extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @var \Spaaza\Loyalty\Model\VoucherInfoSerializer
     */
    protected $voucherInfoSerializer;

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Spaaza\Loyalty\Model\VoucherInfoSerializer $voucherInfoSerializer,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->voucherInfoSerializer = $voucherInfoSerializer;
    }

    protected function _construct()
    {
        $this->_isPkAutoIncrement = false;
        $this->_init('spaaza_quote_address_data', 'quote_address_id');
    }

    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        $this->serializeVouchers($object);
        $object->getVouchers();
        return parent::_beforeSave($object);
    }

    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        $this->unserializeVouchers($object);
        return parent::_afterSave($object);
    }

    /**
     * @param \Spaaza\Loyalty\Model\QuoteAddress\SpaazaData
     */
    private function serializeVouchers($object)
    {
        $vouchers = $object->getData(\Spaaza\Loyalty\Api\Data\QuoteAddress\SpaazaDataInterface::KEY_VOUCHERS);
        $object->setData(
            \Spaaza\Loyalty\Api\Data\QuoteAddress\SpaazaDataInterface::KEY_VOUCHERS,
            $this->voucherInfoSerializer->serializeVouchers($vouchers)
        );
    }

    /**
     * @param \Spaaza\Loyalty\Model\QuoteAddress\SpaazaData
     */
    private function unserializeVouchers($object)
    {
        $vouchers = $object->getData(\Spaaza\Loyalty\Api\Data\QuoteAddress\SpaazaDataInterface::KEY_VOUCHERS);
        $object->setData(
            \Spaaza\Loyalty\Api\Data\QuoteAddress\SpaazaDataInterface::KEY_VOUCHERS,
            $this->voucherInfoSerializer->unserializeVouchers($vouchers)
        );
    }
}
