<?php

namespace Spaaza\Loyalty\Model\ResourceModel\QuoteAddress\SpaazaData;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'quote_address_id';

    protected function _construct()
    {
        $this->_init(
            \Spaaza\Loyalty\Model\QuoteAddress\SpaazaData::class,
            \Spaaza\Loyalty\Model\ResourceModel\QuoteAddress\SpaazaData::class
        );
    }
}
