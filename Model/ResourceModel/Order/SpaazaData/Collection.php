<?php

namespace Spaaza\Loyalty\Model\ResourceModel\Order\SpaazaData;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'order_id';

    protected function _construct()
    {
        $this->_init(
            \Spaaza\Loyalty\Model\Order\SpaazaData::class,
            \Spaaza\Loyalty\Model\ResourceModel\Order\SpaazaData::class
        );
    }
}
