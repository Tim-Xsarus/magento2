<?php

namespace Spaaza\Loyalty\Model\ResourceModel\Invoice;

/**
 * Class SpaazaData
 *
 * @method save(\Spaaza\Loyalty\Api\Data\Invoice\SpaazaDataInterface $object)
 */
class SpaazaData extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_isPkAutoIncrement = false;
        $this->_init('spaaza_invoice_data', 'invoice_id');
    }
}
