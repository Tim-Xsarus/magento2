<?php

namespace Spaaza\Loyalty\Model\ResourceModel\Invoice\SpaazaData;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'invoice_id';

    protected function _construct()
    {
        $this->_init(
            \Spaaza\Loyalty\Model\Invoice\SpaazaData::class,
            \Spaaza\Loyalty\Model\ResourceModel\Invoice\SpaazaData::class
        );
    }
}
