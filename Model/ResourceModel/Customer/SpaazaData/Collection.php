<?php

namespace Spaaza\Loyalty\Model\ResourceModel\Customer\SpaazaData;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'customer_data_id';

    protected function _construct()
    {
        $this->_init(
            \Spaaza\Loyalty\Model\Customer\SpaazaData::class,
            \Spaaza\Loyalty\Model\ResourceModel\Customer\SpaazaData::class
        );
    }
}
