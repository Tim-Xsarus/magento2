<?php

namespace Spaaza\Loyalty\Model\ResourceModel\Customer;

class SpaazaData extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_isPkAutoIncrement = false;
        $this->_init('spaaza_customer_data', 'customer_id');
    }
}
