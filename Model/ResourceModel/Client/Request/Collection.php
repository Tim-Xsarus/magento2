<?php

namespace Spaaza\Loyalty\Model\ResourceModel\Client\Request;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'request_id';

    protected function _construct()
    {
        $this->_init(
            \Spaaza\Loyalty\Model\Client\Request::class,
            \Spaaza\Loyalty\Model\ResourceModel\Client\Request::class
        );
    }
}
