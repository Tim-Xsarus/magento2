<?php

namespace Spaaza\Loyalty\Model\Customer;

use Magento\Framework\Model\AbstractModel;
use Spaaza\Loyalty\Api\Data\Customer\SpaazaDataInterface;

/**
 * @method \Spaaza\Loyalty\Model\ResourceModel\Customer\SpaazaData getResource()
 * @method \Spaaza\Loyalty\Model\ResourceModel\Customer\SpaazaData\Collection getCollection()
 */
class SpaazaData extends AbstractModel implements SpaazaDataInterface
{
    const KEY_CUSTOMER_ID = 'customer_id';
    const KEY_USER_ID = 'user_id';
    const KEY_MEMBER_NUMBER = 'member_number';
    const KEY_PROGRAMME_OPTED_IN = 'programme_opted_in';
    const KEY_MAILING_LIST_SUBSCRIBED = 'mailing_list_subscribed';
    const KEY_PRINTED_MAILING_LIST_SUBSCRIBED = 'printed_mailing_list_subscribed';
    const KEY_LAST_HASH = 'last_hash';

    protected function _construct()
    {
        $this->_init(\Spaaza\Loyalty\Model\ResourceModel\Customer\SpaazaData::class);
    }

    /**
     * Get Customer Id
     *
     * @return int
     */
    public function getCustomerId(): ?int
    {
        return $this->getData(self::KEY_CUSTOMER_ID) !== null
            ? (int)$this->getData(self::KEY_CUSTOMER_ID)
            : null;
    }

    /**
     * Set Customer Id
     *
     * @param int|null $customerId
     * @return $this
     */
    public function setCustomerId(?int $customerId)
    {
        return $this->setData(self::KEY_CUSTOMER_ID, $customerId);
    }

    /**
     * Get User Id
     *
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->getData(self::KEY_USER_ID) !== null
            ? (int)$this->getData(self::KEY_USER_ID)
            : null;
    }

    /**
     * Set User Id
     *
     * @param int|null $userId
     * @return $this
     */
    public function setUserId(?int $userId)
    {
        return $this->setData(self::KEY_USER_ID, $userId);
    }

    /**
     * Get Member Number
     *
     * @return string|null
     */
    public function getMemberNumber(): ?string
    {
        return $this->getData(self::KEY_MEMBER_NUMBER);
    }

    /**
     * Set Member Number
     *
     * @param string|null $memberNumber
     * @return $this
     */
    public function setMemberNumber(?string $memberNumber)
    {
        return $this->setData(self::KEY_MEMBER_NUMBER, $memberNumber);
    }

    /**
     * Get Programme Opted In
     *
     * @return bool|null
     */
    public function getProgrammeOptedIn(): ?bool
    {
        return $this->getData(self::KEY_PROGRAMME_OPTED_IN) !== null
            ? (bool)$this->getData(self::KEY_PROGRAMME_OPTED_IN)
            : null;
    }

    /**
     * Set Programme Opted In
     *
     * @param bool|null $programmeOptedIn
     * @return $this
     */
    public function setProgrammeOptedIn(?bool $programmeOptedIn)
    {
        return $this->setData(self::KEY_PROGRAMME_OPTED_IN, $programmeOptedIn);
    }

    /**
     * Get Mailing List Subscribed
     *
     * @return bool|null
     */
    public function getMailingListSubscribed(): ?bool
    {
        return $this->getData(self::KEY_MAILING_LIST_SUBSCRIBED) !== null
            ? (bool)$this->getData(self::KEY_MAILING_LIST_SUBSCRIBED)
            : null;
    }

    /**
     * Set Mailing List Subscribed
     *
     * @param bool|null $mailingListSubscribed
     * @return $this
     */
    public function setMailingListSubscribed(?bool $mailingListSubscribed)
    {
        return $this->setData(self::KEY_MAILING_LIST_SUBSCRIBED, $mailingListSubscribed);
    }

    /**
     * Get Printed Mailing List Subscribed
     *
     * @return bool|null
     */
    public function getPrintedMailingListSubscribed(): ?bool
    {
        return $this->getData(self::KEY_PRINTED_MAILING_LIST_SUBSCRIBED) !== null
            ? (bool)$this->getData(self::KEY_PRINTED_MAILING_LIST_SUBSCRIBED)
            : null;
    }

    /**
     * Set Printed Mailing List Subscribed
     *
     * @param bool|null $printedMailingListSubscribed
     * @return $this
     */
    public function setPrintedMailingListSubscribed(?bool $printedMailingListSubscribed)
    {
        return $this->setData(self::KEY_PRINTED_MAILING_LIST_SUBSCRIBED, $printedMailingListSubscribed);
    }

    /**
     * Get Last Hash
     *
     * @return string|null
     */
    public function getLastHash(): ?string
    {
        return $this->getData(self::KEY_LAST_HASH);
    }

    /**
     * Set Last Hash
     *
     * @param string|null $lastHash
     * @return $this
     */
    public function setLastHash(?string $lastHash)
    {
        return $this->setData(self::KEY_LAST_HASH, $lastHash);
    }
}
