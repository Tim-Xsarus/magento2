<?php

namespace Spaaza\Loyalty\Model\Customer;

use Magento\Customer\Api\Data\CustomerInterface as CustomerData;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Spaaza\Loyalty\Api\Data\Customer\SpaazaDataInterface;

class SpaazaDataManagement
{
    /**
     * @var \Magento\Customer\Api\Data\CustomerExtensionFactory
     */
    protected $customerExtensionFactory;

    /**
     * @var \Spaaza\Loyalty\Api\Data\Customer\SpaazaDataInterfaceFactory
     */
    protected $spaazaDataFactory;

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    protected $helper;

    /**
     * @var \Spaaza\Loyalty\Model\ResourceModel\Customer\SpaazaData
     */
    protected $spaazaDataResourceModel;

    protected $doNotSyncOnSave;

    /**
     * SpaazaDataManagement constructor.
     *
     * @param \Spaaza\Loyalty\Helper\Data $helper
     * @param \Spaaza\Loyalty\Api\Data\Customer\SpaazaDataInterfaceFactory $spaazaDataFactory
     * @param \Magento\Customer\Api\Data\CustomerExtensionFactory $customerExtensionFactory
     * @param \Spaaza\Loyalty\Model\ResourceModel\Customer\SpaazaData $spaazaDataResourceModel
     */
    public function __construct(
        \Spaaza\Loyalty\Helper\Data $helper,
        \Spaaza\Loyalty\Api\Data\Customer\SpaazaDataInterfaceFactory $spaazaDataFactory,
        \Magento\Customer\Api\Data\CustomerExtensionFactory $customerExtensionFactory,
        \Spaaza\Loyalty\Model\ResourceModel\Customer\SpaazaData $spaazaDataResourceModel
    ) {
        $this->helper = $helper;
        $this->customerExtensionFactory = $customerExtensionFactory;
        $this->spaazaDataFactory = $spaazaDataFactory;
        $this->spaazaDataResourceModel = $spaazaDataResourceModel;
    }

    /**
     * Ensures existing extension attributes and (loaded) Spaaza data
     *
     * @param CustomerData $customer
     * @return SpaazaDataInterface
     * @throws LocalizedException
     */
    public function applyExtensionAttributes(CustomerData $customer)
    {
        $extensionAttributes = $customer->getExtensionAttributes();
        if (!$extensionAttributes) {
            $extensionAttributes = $this->customerExtensionFactory->create();
            $this->helper->debugLog(
                'Added extension attributes object to customer',
                ['customer' => $customer->getId() ?: 'new']
            );
        }

        $spaazaData = $extensionAttributes->getSpaazaData();
        if (!$spaazaData) {
            if ($customer->getId()) {
                try {
                    $spaazaData = $this->getByCustomerId($customer->getId());
                    $this->helper->debugLog(
                        'Got extension attribute from database',
                        ['customer' => $customer->getId(), 'hash' => $spaazaData->getLastHash()]
                    );
                } catch (NoSuchEntityException $e) {
                    $spaazaData = $this->spaazaDataFactory->create();
                    $spaazaData->setCustomerId($customer->getId());
                    $this->helper->debugLog(
                        'Created a new extension attribute for existing customer',
                        ['customer' => $customer->getId()]
                    );
                }
            } else {
                $spaazaData = $this->spaazaDataFactory->create();
                $this->helper->debugLog('Created a new extension attribute for new customer');
            }
        }

        $extensionAttributes->setSpaazaData($spaazaData);
        $customer->setExtensionAttributes($extensionAttributes);

        return $spaazaData;
    }

    /**
     * Retrieve Customer SpaazaData By Customer Id
     *
     * @param $customerId
     * @return SpaazaDataInterface
     * @throws LocalizedException
     */
    public function getByCustomerId($customerId)
    {
        $spaazaData = $this->spaazaDataFactory->create();
        $this->spaazaDataResourceModel->load($spaazaData, $customerId);
        if (!$spaazaData->getCustomerId()) {
            throw new NoSuchEntityException(__('SpaazaData for customer id "%1" does not exist.', $customerId));
        }
        return $spaazaData;
    }

    /**
     * Save Customer SpaazaData
     *
     * @param SpaazaDataInterface $data
     * @return SpaazaDataInterface
     * @throws LocalizedException
     */
    public function save(SpaazaDataInterface $data)
    {
        if (!$data->getCustomerId()) {
            throw new CouldNotSaveException(__('Cannot save customer Spaaza data without a customer id'));
        }
        try {
            $this->helper->debugLog(
                'Save extension attribute',
                [
                    'customer_id' => $data->getCustomerId(),
                    'user_id' => $data->getUserId(),
                    'hash' => $data->getLastHash()
                ]
            );
            $this->spaazaDataResourceModel->save($data);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__('Could not save customer Spaaza data: %1', $exception->getMessage()));
        }
        return $data;
    }

    /**
     * Set 'do not sync on save' flag for a customer
     *
     * @param CustomerData $customer
     * @return void
     */
    public function setDoNotSyncOnSave(CustomerData $customer)
    {
        if ($customer->getId()) {
            $this->doNotSyncOnSave[$customer->getId()] = true;
        }
        $this->doNotSyncOnSave[spl_object_hash($customer)] = true;
    }

    /**
     * Reset 'do not sync on save' flag for a customer
     *
     * @param CustomerData $customer
     * @return void
     */
    public function resetDoNotSyncOnSave(CustomerData $customer)
    {
        unset($this->doNotSyncOnSave[$customer->getId()]);
        unset($this->doNotSyncOnSave[spl_object_hash($customer)]);
    }

    /**
     * Get 'do not sync on save' flag for a customer (based on id or spl object hash for new customers)
     *
     * @param CustomerData $customer
     * @return bool
     */
    public function getDoNotSyncOnSave(CustomerData $customer)
    {
        if ($customer->getId() && !empty($this->doNotSyncOnSave[$customer->getId()])) {
            return true;
        }
        if (!empty($this->doNotSyncOnSave[spl_object_hash($customer)])) {
            return true;
        }
        return false;
    }

    /**
     * Find a customer id by Spaaza user id
     *
     * @param int $spaazaUserId
     * @return int|null  Customer id or NULL if not found
     * @throws LocalizedException
     */
    public function findCustomerIdBySpaazaUserId($spaazaUserId)
    {
        $spaazaData = $this->spaazaDataFactory->create();
        $this->spaazaDataResourceModel->load($spaazaData, $spaazaUserId, 'user_id');
        return $spaazaData->getCustomerId();
    }

    /**
     * Find a customer id by Spaaza member number
     *
     * @param string $spaazaMemberNumber
     * @return int|null  Customer id or NULL if not found
     * @throws LocalizedException
     */
    public function findCustomerIdBySpaazaMemberNumber($spaazaMemberNumber)
    {
        $spaazaData = $this->spaazaDataFactory->create();
        $this->spaazaDataResourceModel->load($spaazaData, $spaazaMemberNumber, 'member_number');
        return $spaazaData->getCustomerId();
    }
}
