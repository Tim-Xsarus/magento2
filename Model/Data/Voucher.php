<?php

namespace Spaaza\Loyalty\Model\Data;

use Magento\Framework\Api\AbstractSimpleObject;
use Spaaza\Loyalty\Api\Data\VoucherInterface;

class Voucher extends AbstractSimpleObject implements VoucherInterface
{

    /**
     * {@inheritdoc}
     */
    public function getVoucherKey(): ?string
    {
        return $this->_get(self::KEY_VOUCHER_KEY);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherKey(string $voucherKey)
    {
        return $this->setData(self::KEY_VOUCHER_KEY, $voucherKey);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherId(): ?int
    {
        return $this->_get(self::KEY_VOUCHER_ID) !== null
            ? (int)$this->_get(self::KEY_VOUCHER_ID)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherId(?int $voucherId)
    {
        return $this->setData(self::KEY_VOUCHER_ID, $voucherId);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherStatus(): ?string
    {
        return $this->_get(self::KEY_VOUCHER_STATUS);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherStatus(string $voucherStatus)
    {
        return $this->setData(self::KEY_VOUCHER_STATUS, $voucherStatus);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherLocked(): bool
    {
        return (bool)$this->_get(self::KEY_VOUCHER_LOCKED);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherLocked(bool $voucherLocked)
    {
        return $this->setData(self::KEY_VOUCHER_LOCKED, $voucherLocked);
    }

    /**
     * {@inheritdoc}
     */
    public function getCampaignId(): ?int
    {
        return $this->_get(self::KEY_CAMPAIGN_ID) !== null
            ? (int)$this->_get(self::KEY_CAMPAIGN_ID)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setCampaignId(int $campaignId)
    {
        return $this->setData(self::KEY_CAMPAIGN_ID, $campaignId);
    }

    /**
     * {@inheritdoc}
     */
    public function getCampaignType(): ?string
    {
        return $this->_get(self::KEY_CAMPAIGN_TYPE);
    }

    /**
     * {@inheritdoc}
     */
    public function setCampaignType(?string $campaignType)
    {
        return $this->setData(self::KEY_CAMPAIGN_TYPE, $campaignType);
    }

    /**
     * {@inheritdoc}
     */
    public function getCampaignTitle(): ?string
    {
        return $this->_get(self::KEY_CAMPAIGN_TITLE);
    }

    /**
     * {@inheritdoc}
     */
    public function setCampaignTitle(?string $campaignTitle)
    {
        return $this->setData(self::KEY_CAMPAIGN_TITLE, $campaignTitle);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherExpiryDatetimeUtc(): ?string
    {
        return $this->_get(self::KEY_VOUCHER_EXPIRY_DATETIME_UTC);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherExpiryDatetimeUtc(?string $voucherExpiryDatetimeUtc)
    {
        return $this->setData(self::KEY_VOUCHER_EXPIRY_DATETIME_UTC, $voucherExpiryDatetimeUtc);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherExpirySecondsRemaining()
    {
        return $this->_get(self::KEY_VOUCHER_EXPIRY_SECONDS_REMAINING) !== null
            ? (int)$this->_get(self::KEY_VOUCHER_EXPIRY_SECONDS_REMAINING)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherExpirySecondsRemaining(?int $voucherExpirySecondsRemaining)
    {
        return $this->setData(self::KEY_VOUCHER_EXPIRY_SECONDS_REMAINING, $voucherExpirySecondsRemaining);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherCurrencyId(): ?int
    {
        return $this->_get(self::KEY_VOUCHER_CURRENCY_ID) !== null
            ? (int)$this->_get(self::KEY_VOUCHER_CURRENCY_ID)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherCurrencyId(int $voucherCurrencyId)
    {
        return $this->setData(self::KEY_VOUCHER_CURRENCY_ID, $voucherCurrencyId);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherCurrencySymbol(): ?string
    {
        return $this->_get(self::KEY_VOUCHER_CURRENCY_SYMBOL);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherCurrencySymbol(string $voucherCurrencySymbol)
    {
        return $this->setData(self::KEY_VOUCHER_CURRENCY_SYMBOL, $voucherCurrencySymbol);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherAmountOriginal(): ?float
    {
        return $this->_get(self::KEY_VOUCHER_AMOUNT_ORIGINAL) !== null
            ? (float)$this->_get(self::KEY_VOUCHER_AMOUNT_ORIGINAL)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherAmountOriginal(?float $voucherAmountOriginal)
    {
        return $this->setData(self::KEY_VOUCHER_AMOUNT_ORIGINAL, $voucherAmountOriginal);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherAmountRedeemed(): float
    {
        return (float)$this->_get(self::KEY_VOUCHER_AMOUNT_REDEEMED);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherAmountRedeemed(float $voucherAmountRedeemed)
    {
        return $this->setData(self::KEY_VOUCHER_AMOUNT_REDEEMED, (float)$voucherAmountRedeemed);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherAmount(): ?float
    {
        return $this->_get(self::KEY_VOUCHER_AMOUNT) !== null
            ? (float)$this->_get(self::KEY_VOUCHER_AMOUNT)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherAmount(float $voucherAmount)
    {
        return $this->setData(self::KEY_VOUCHER_AMOUNT, $voucherAmount);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherText(): ?string
    {
        return $this->_get(self::KEY_VOUCHER_TEXT);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherText(?string $voucherText)
    {
        return $this->setData(self::KEY_VOUCHER_TEXT, $voucherText);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherLockingCode(): ?string
    {
        return $this->_get(self::KEY_VOUCHER_LOCKING_CODE);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherLockingCode(?string $voucherLockingCode)
    {
        return $this->setData(self::KEY_VOUCHER_LOCKING_CODE, $voucherLockingCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherBasketOwnerCodeExclusive(): ?string
    {
        return $this->_get(self::KEY_VOUCHER_BASKET_OWNER_CODE_EXCLUSIVE);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherBasketOwnerCodeExclusive(?string $voucherBasketOwnerCodeExclusive)
    {
        return $this->setData(self::KEY_VOUCHER_BASKET_OWNER_CODE_EXCLUSIVE, $voucherBasketOwnerCodeExclusive);
    }

    /**
     * @inheritDoc
     */
    public function getVoucherType(): ?string
    {
        return $this->_get(self::KEY_VOUCHER_TYPE);
    }

    /**
     * @inheritDoc
     */
    public function setVoucherType(string $voucherType)
    {
        return $this->setData(self::KEY_VOUCHER_TYPE, $voucherType);
    }

    /**
     * @inheritDoc
     */
    public function getVoucherHonourCode(): ?string
    {
        return $this->_get(self::KEY_VOUCHER_HONOUR_CODE);
    }

    /**
     * @inheritDoc
     */
    public function setVoucherHonourCode(?string $voucherHonourCode)
    {
        return $this->setData(self::KEY_VOUCHER_HONOUR_CODE, $voucherHonourCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherCreatedDatetime(): ?string
    {
        return $this->_get(self::KEY_VOUCHER_CREATED_DATETIME);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherCreatedDatetime(?string $voucherCreatedDatetime)
    {
        return $this->setData(self::KEY_VOUCHER_CREATED_DATETIME, $voucherCreatedDatetime);
    }

    /**
     * {@inheritdoc}
     */
    public function getCampaignImageFilename(): ?string
    {
        return $this->_get(self::KEY_CAMPAIGN_IMAGE_FILENAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setCampaignImageFilename(?string $campaignImageFilename)
    {
        return $this->setData(self::KEY_CAMPAIGN_IMAGE_FILENAME, $campaignImageFilename);
    }

    /**
     * {@inheritdoc}
     */
    public function getCampaignImageUrl(): ?string
    {
        return $this->_get(self::KEY_CAMPAIGN_IMAGE_URL);
    }

    /**
     * {@inheritdoc}
     */
    public function setCampaignImageUrl(?string $campaignImageUrl)
    {
        return $this->setData(self::KEY_CAMPAIGN_IMAGE_URL, $campaignImageUrl);
    }

    /**
     * {@inheritdoc}
     */
    public function getCampaignImageLink(): ?string
    {
        return $this->_get(self::KEY_CAMPAIGN_IMAGE_LINK);
    }

    /**
     * {@inheritdoc}
     */
    public function setCampaignImageLink(?string $campaignImageLink)
    {
        return $this->setData(self::KEY_CAMPAIGN_IMAGE_LINK, $campaignImageLink);
    }

    /**
     * {@inheritdoc}
     */
    public function getCampaignImageDimensionX(): ?int
    {
        return $this->_get(self::KEY_CAMPAIGN_IMAGE_DIMENSION_X) !== null
            ? (int)$this->_get(self::KEY_CAMPAIGN_IMAGE_DIMENSION_X)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setCampaignImageDimensionX(?int $campaignImageDimensionX)
    {
        return $this->setData(self::KEY_CAMPAIGN_IMAGE_DIMENSION_X, $campaignImageDimensionX);
    }

    /**
     * {@inheritdoc}
     */
    public function getCampaignImageDimensionY(): ?int
    {
        return $this->_get(self::KEY_CAMPAIGN_IMAGE_DIMENSION_Y) !== null
            ? (int)$this->_get(self::KEY_CAMPAIGN_IMAGE_DIMENSION_Y)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setCampaignImageDimensionY(?int $campaignImageDimensionY)
    {
        return $this->setData(self::KEY_CAMPAIGN_IMAGE_DIMENSION_Y, $campaignImageDimensionY);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherRedeemedDatetime(): ?string
    {
        return $this->_get(self::KEY_VOUCHER_REDEEMED_DATETIME);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherRedeemedDatetime(?string $voucherRedeemedDatetime)
    {
        return $this->setData(self::KEY_VOUCHER_REDEEMED_DATETIME, $voucherRedeemedDatetime);
    }

    /**
     * {@inheritdoc}
     */
    public function getParentVoucher(): ?string
    {
        return $this->_get(self::KEY_PARENT_VOUCHER);
    }

    /**
     * {@inheritdoc}
     */
    public function setParentVoucher(?string $parentVoucher)
    {
        return $this->setData(self::KEY_PARENT_VOUCHER, $parentVoucher);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherTitle(): ?string
    {
        return $this->_get(self::KEY_VOUCHER_TITLE);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherTitle(?string $voucherTitle)
    {
        return $this->setData(self::KEY_VOUCHER_TITLE, $voucherTitle);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherDescription(): ?string
    {
        return $this->_get(self::KEY_VOUCHER_DESCRIPTION);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherDescription(?string $voucherDescription)
    {
        return $this->setData(self::KEY_VOUCHER_DESCRIPTION, $voucherDescription);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherNotes(): ?string
    {
        return $this->_get(self::KEY_VOUCHER_NOTES);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherNotes(?string $voucherNotes)
    {
        return $this->setData(self::KEY_VOUCHER_NOTES, $voucherNotes);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherImageUrl(): ?string
    {
        return $this->_get(self::KEY_VOUCHER_IMAGE_URL);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherImageUrl(?string $voucherImageUrl)
    {
        return $this->setData(self::KEY_VOUCHER_IMAGE_URL, $voucherImageUrl);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherDiscountRatio(): ?float
    {
        return $this->_get(self::KEY_VOUCHER_DISCOUNT_RATIO) !== null
            ? (float)$this->_get(self::KEY_VOUCHER_DISCOUNT_RATIO)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherDiscountRatio(?float $voucherDiscountRatio)
    {
        return $this->setData(self::KEY_VOUCHER_DISCOUNT_RATIO, $voucherDiscountRatio);
    }
}
