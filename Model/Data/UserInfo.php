<?php

namespace Spaaza\Loyalty\Model\Data;

use Magento\Framework\Api\AbstractSimpleObject;
use Spaaza\Loyalty\Api\Data\UserInfoInterface;

class UserInfo extends AbstractSimpleObject implements UserInfoInterface
{

    /**
     * {@inheritdoc}
     */
    public function getFirstName(): ?string
    {
        return $this->_get(self::KEY_FIRST_NAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setFirstName(string $firstName)
    {
        return $this->setData(self::KEY_FIRST_NAME, $firstName);
    }

    /**
     * {@inheritdoc}
     */
    public function getLastName(): ?string
    {
        return $this->_get(self::KEY_LAST_NAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setLastName(?string $lastName)
    {
        return $this->setData(self::KEY_LAST_NAME, $lastName);
    }

    /**
     * {@inheritdoc}
     */
    public function getBirthday(): ?string
    {
        return $this->_get(self::KEY_BIRTHDAY);
    }

    /**
     * {@inheritdoc}
     */
    public function setBirthday(?string $birthday)
    {
        return $this->setData(self::KEY_BIRTHDAY, $birthday);
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername(): ?string
    {
        return $this->_get(self::KEY_USERNAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setUsername(string $username)
    {
        return $this->setData(self::KEY_USERNAME, $username);
    }

    /**
     * {@inheritdoc}
     */
    public function getWebshopCustomerId(): ?int
    {
        return $this->_get(self::KEY_WEBSHOP_CUSTOMER_ID) !== null
            ? (int)$this->_get(self::KEY_WEBSHOP_CUSTOMER_ID)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setWebshopCustomerId(?int $webshopCustomerId)
    {
        return $this->setData(self::KEY_WEBSHOP_CUSTOMER_ID, $webshopCustomerId);
    }

    /**
     * {@inheritdoc}
     */
    public function getGender(): ?string
    {
        return $this->_get(self::KEY_GENDER);
    }

    /**
     * {@inheritdoc}
     */
    public function setGender(?string $gender)
    {
        return $this->setData(self::KEY_GENDER, $gender);
    }

    /**
     * {@inheritdoc}
     */
    public function getUserId(): ?int
    {
        return $this->_get(self::KEY_USER_ID) !== null
            ? (int)$this->_get(self::KEY_USER_ID)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setUserId(int $userId)
    {
        return $this->setData(self::KEY_USER_ID, $userId);
    }

    /**
     * {@inheritdoc}
     */
    public function getPrintedMailingListSubscribed(): ?bool
    {
        return $this->_get(self::KEY_PRINTED_MAILING_LIST_SUBSCRIBED) !== null
            ? (bool)$this->_get(self::KEY_PRINTED_MAILING_LIST_SUBSCRIBED)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setPrintedMailingListSubscribed(?bool $printedMailingListSubscribed)
    {
        return $this->setData(self::KEY_PRINTED_MAILING_LIST_SUBSCRIBED, $printedMailingListSubscribed);
    }

    /**
     * {@inheritdoc}
     */
    public function getMailingListSubscribed(): ?bool
    {
        return $this->_get(self::KEY_MAILING_LIST_SUBSCRIBED) !== null
            ? (bool)$this->_get(self::KEY_MAILING_LIST_SUBSCRIBED)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setMailingListSubscribed(?bool $mailingListSubscribed)
    {
        return $this->setData(self::KEY_MAILING_LIST_SUBSCRIBED, $mailingListSubscribed);
    }

    /**
     * {@inheritdoc}
     */
    public function getMailingListSubOffered(): ?bool
    {
        return $this->_get(self::KEY_MAILING_LIST_SUB_OFFERED) !== null
            ? (bool)$this->_get(self::KEY_MAILING_LIST_SUB_OFFERED)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setMailingListSubOffered(?bool $mailingListSubOffered)
    {
        return $this->setData(self::KEY_MAILING_LIST_SUB_OFFERED, $mailingListSubOffered);
    }

    /**
     * {@inheritdoc}
     */
    public function getProgrammeOptedIn(): ?bool
    {
        return $this->_get(self::KEY_PROGRAMME_OPTED_IN) !== null
            ? (bool)$this->_get(self::KEY_PROGRAMME_OPTED_IN)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setProgrammeOptedIn(?bool $programmeOptedIn)
    {
        return $this->setData(self::KEY_PROGRAMME_OPTED_IN, $programmeOptedIn);
    }

    /**
     * {@inheritdoc}
     */
    public function getAddressStreetname(): ?string
    {
        return $this->_get(self::KEY_ADDRESS_STREETNAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setAddressStreetname(?string $addressStreetname)
    {
        return $this->setData(self::KEY_ADDRESS_STREETNAME, $addressStreetname);
    }

    /**
     * {@inheritdoc}
     */
    public function getAddressHousenumber(): ?string
    {
        return $this->_get(self::KEY_ADDRESS_HOUSENUMBER);
    }

    /**
     * {@inheritdoc}
     */
    public function setAddressHousenumber(?string $addressHousenumber)
    {
        return $this->setData(self::KEY_ADDRESS_HOUSENUMBER, $addressHousenumber);
    }

    /**
     * {@inheritdoc}
     */
    public function getAddressHousenumberExtension(): ?string
    {
        return $this->_get(self::KEY_ADDRESS_HOUSENUMBER_EXTENSION);
    }

    /**
     * {@inheritdoc}
     */
    public function setAddressHousenumberExtension(?string $addressHousenumberExtension)
    {
        return $this->setData(self::KEY_ADDRESS_HOUSENUMBER_EXTENSION, $addressHousenumberExtension);
    }

    /**
     * {@inheritdoc}
     */
    public function getAddressPostalcode(): ?string
    {
        return $this->_get(self::KEY_ADDRESS_POSTALCODE);
    }

    /**
     * {@inheritdoc}
     */
    public function setAddressPostalcode(?string $addressPostalcode)
    {
        return $this->setData(self::KEY_ADDRESS_POSTALCODE, $addressPostalcode);
    }

    /**
     * {@inheritdoc}
     */
    public function getAddressTowncity(): ?string
    {
        return $this->_get(self::KEY_ADDRESS_TOWNCITY);
    }

    /**
     * {@inheritdoc}
     */
    public function setAddressTowncity(?string $addressTowncity)
    {
        return $this->setData(self::KEY_ADDRESS_TOWNCITY, $addressTowncity);
    }

    /**
     * {@inheritdoc}
     */
    public function getCountryCode(): ?string
    {
        return $this->_get(self::KEY_COUNTRY_CODE);
    }

    /**
     * {@inheritdoc}
     */
    public function setCountryCode(?string $countryCode)
    {
        return $this->setData(self::KEY_COUNTRY_CODE, $countryCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getPhoneNumber(): ?string
    {
        return $this->_get(self::KEY_PHONE_NUMBER);
    }

    /**
     * {@inheritdoc}
     */
    public function setPhoneNumber(?string $phoneNumber)
    {
        return $this->setData(self::KEY_PHONE_NUMBER, $phoneNumber);
    }
}
