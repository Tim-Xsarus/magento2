<?php

namespace Spaaza\Loyalty\Model\Data;

use Magento\Framework\Api\AbstractSimpleObject;
use Spaaza\Loyalty\Api\Data\AuthenticationResultInterface;

class AuthenticationResult extends AbstractSimpleObject implements AuthenticationResultInterface
{
    const AUTHENTICATED = 'authenticated';
    const CUSTOMER_ID = 'customer_id';
    const MESSAGE = 'message';

    /**
     * {@inheritdoc}
     */
    public function getAuthenticated()
    {
        return (bool)$this->_get(self::AUTHENTICATED);
    }

    /**
     * {@inheritdoc}
     */
    public function setAuthenticated($authenticated)
    {
        return $this->setData(self::AUTHENTICATED, (bool)$authenticated);
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomerId()
    {
        return $this->_get(self::CUSTOMER_ID) !== null
            ? (int)$this->_get(self::CUSTOMER_ID)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomerId(?int $customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * {@inheritdoc}
     */
    public function getMessage()
    {
        return $this->_get(self::MESSAGE) !== null
            ? (string)$this->_get(self::MESSAGE)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setMessage(?string $message)
    {
        return $this->setData(self::MESSAGE, $message);
    }
}
