<?php

namespace Spaaza\Loyalty\Model\Order;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;
use Spaaza\Loyalty\Api\Data\Order\SpaazaDataInterface;

class SpaazaDataManagement
{
    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    private $helper;

    /**
     * @var \Spaaza\Loyalty\Api\Data\Order\SpaazaDataInterfaceFactory
     */
    private $spaazaDataFactory;

    /**
     * @var \Magento\Sales\Api\Data\OrderExtensionFactory
     */
    private $orderExtensionFactory;

    /**
     * @var \Spaaza\Loyalty\Model\ResourceModel\Order\SpaazaData
     */
    private $spaazaDataResourceModel;

    /**
     * SpaazaDataManagement constructor.
     *
     * @param \Spaaza\Loyalty\Helper\Data $helper
     * @param \Spaaza\Loyalty\Api\Data\Order\SpaazaDataInterfaceFactory $spaazaDataFactory
     * @param \Magento\Sales\Api\Data\OrderExtensionFactory $orderExtensionFactory
     * @param \Spaaza\Loyalty\Model\ResourceModel\Order\SpaazaData $spaazaDataResourceModel
     */
    public function __construct(
        \Spaaza\Loyalty\Helper\Data $helper,
        \Spaaza\Loyalty\Api\Data\Order\SpaazaDataInterfaceFactory $spaazaDataFactory,
        \Magento\Sales\Api\Data\OrderExtensionFactory $orderExtensionFactory,
        \Spaaza\Loyalty\Model\ResourceModel\Order\SpaazaData $spaazaDataResourceModel
    ) {
        $this->helper = $helper;
        $this->spaazaDataFactory = $spaazaDataFactory;
        $this->orderExtensionFactory = $orderExtensionFactory;
        $this->spaazaDataResourceModel = $spaazaDataResourceModel;
    }

    /**
     * Ensures existing extension attributes and (loaded) Spaaza data
     *
     * @param OrderInterface $order
     * @return SpaazaDataInterface
     */
    public function applyExtensionAttributes(OrderInterface $order)
    {
        $extensionAttributes = $order->getExtensionAttributes();
        if (!$extensionAttributes) {
            $extensionAttributes = $this->orderExtensionFactory->create();
            $this->helper->debugLog(
                'Added extension attributes object to order',
                ['order' => $order->getEntityId() ?: 'new']
            );
        }

        $spaazaData = $extensionAttributes->getSpaazaData();
        if (!$spaazaData) {
            if ($order->getEntityId()) {
                try {
                    $spaazaData = $this->getByOrderId($order->getEntityId());
                    $this->helper->debugLog(
                        'Got extension attribute from database',
                        ['order' => $order->getEntityId()]
                    );
                } catch (NoSuchEntityException $e) {
                    $spaazaData = $this->spaazaDataFactory->create();
                    $spaazaData->setOrderId($order->getEntityId());
                    $this->helper->debugLog(
                        'Created a new extension attribute for existing order',
                        ['order' => $order->getEntityId()]
                    );
                }
            } else {
                $spaazaData = $this->spaazaDataFactory->create();
                $this->helper->debugLog('Created a new extension attribute for new order');
            }
        }

        $extensionAttributes->setSpaazaData($spaazaData);
        $order->setExtensionAttributes($extensionAttributes);

        return $spaazaData;
    }

    /**
     * Retrieve Order SpaazaData By Order Id
     *
     * @param int $orderId
     * @return SpaazaDataInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByOrderId($orderId)
    {
        $spaazaData = $this->spaazaDataFactory->create();
        $this->spaazaDataResourceModel->load($spaazaData, $orderId);
        if (!$spaazaData->getOrderId()) {
            throw new NoSuchEntityException(__('SpaazaData for order id "%1" does not exist.', $orderId));
        }
        return $spaazaData;
    }

    /**
     * Save Order SpaazaData
     *
     * @param SpaazaDataInterface $data
     * @return SpaazaDataInterface
     * @throws CouldNotSaveException
     */
    public function save(SpaazaDataInterface $data)
    {
        if (!$data->getOrderId()) {
            throw new CouldNotSaveException(__('Cannot save order Spaaza data without an order id'));
        }
        try {
            $this->helper->debugLog(
                'Save extension attribute',
                ['order' => $data->getOrderId(), 'basket' => $data->getBasketId()]
            );
            $this->spaazaDataResourceModel->save($data);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__('Could not save order Spaaza data: %1', $exception->getMessage()));
        }
        return $data;
    }
}
