<?php

namespace Spaaza\Loyalty\Model;

use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Exception\NoSuchEntityException;
use Spaaza\Loyalty\Api\CustomerManagementInterface;

class CustomerManagement implements CustomerManagementInterface
{
    /**
     * @var \Magento\Customer\Api\AccountManagementInterface
     */
    protected $accountManagement;

    /**
     * @var \Spaaza\Loyalty\Api\Data\AuthenticationResultInterfaceFactory
     */
    protected $authenticationResultFactory;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilderFactory;

    /**
     * @var \Spaaza\Loyalty\Model\Connector\Customer
     */
    protected $customerConnector;

    /**
     * @var \Magento\Customer\Api\Data\CustomerInterfaceFactory
     */
    protected $customerFactory;

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $encryptor;

    /**
     * @var Customer\SpaazaDataManagement
     */
    protected $spaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Api\Data\UserInfoInterfaceFactory
     */
    private $userInfoInterfaceFactory;

    public function __construct(
        \Magento\Customer\Api\AccountManagementInterface $accountManagement,
        \Spaaza\Loyalty\Api\Data\AuthenticationResultInterfaceFactory $authenticationResultFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Api\SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Customer\Api\Data\CustomerInterfaceFactory $customerFactory,
        \Spaaza\Loyalty\Model\Connector\Customer $customerConnector,
        \Spaaza\Loyalty\Api\Data\UserInfoInterfaceFactory $userInfoInterfaceFactory,
        \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement $spaazaDataManagement
    ) {
        $this->accountManagement = $accountManagement;
        $this->authenticationResultFactory = $authenticationResultFactory;
        $this->customerRepository = $customerRepository;
        $this->customerFactory = $customerFactory;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
        $this->customerConnector = $customerConnector;
        $this->encryptor = $encryptor;
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->userInfoInterfaceFactory = $userInfoInterfaceFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function authenticate(string $email, string $password)
    {
        $result = $this->authenticationResultFactory->create();
        $result->setAuthenticated(false);

        try {
            $customer = $this->accountManagement->authenticate($email, $password);
            if ($customer->getId()) {
                $result->setAuthenticated(true)
                    ->setCustomerId($customer->getId());
            }
        } catch (AuthenticationException $e) {
            $result->setMessage($e->getMessage());
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function update(array $userInfo, bool $createIfNotExists = false)
    {
        $customerId = null;

        if (!empty($userInfo['user_id'])) {
            $customerId = $this->spaazaDataManagement->findCustomerIdBySpaazaUserId($userInfo['user_id']);
        } elseif (!empty($userInfo['member_number'])) {
            $customerId = $this->spaazaDataManagement->findCustomerIdBySpaazaMemberNumber($userInfo['member_number']);
        }
        if ($customerId) {
            $customer = $this->customerRepository->getById($customerId);
        } else {
            $customer = null;
            if (!empty($userInfo['authentication_point_identifier'])) {
                try {
                    $customer = $this->customerRepository->getById($userInfo['authentication_point_identifier']);
                    // phpcs:ignore Magento2.Exceptions.ThrowCatch
                } catch (NoSuchEntityException $e) {
                    $customer = null;
                }
            }
            if (empty($customer) && !empty($userInfo['username'])) {
                try {
                    $customer = $this->customerRepository->get($userInfo['username']);
                    // phpcs:ignore Magento2.Exceptions.ThrowCatch
                } catch (NoSuchEntityException $e) {
                    $customer = null;
                }
            }
        }

        if (empty($customer) && $createIfNotExists) {
            $customer = $this->customerFactory->create();
        }

        if (!empty($customer)) {
            $resultCustomer = $this->customerConnector->updateCustomerFromSpaazaUserInfo($customer, $userInfo);

            if (!empty($userInfo['password'])) {
                $passwordHash = $this->createPasswordHash($userInfo['password']);
                $resultCustomer = $this->customerRepository->save($resultCustomer, $passwordHash);
            }

            if (isset($resultCustomer)) {
                // get the customer from the database again for if the address has changed
                $resultCustomer = $this->customerRepository->getById($resultCustomer->getId());
                return $resultCustomer;
            }
        } else {
            throw new NoSuchEntityException(__('Customer not found'));
        }
        return null;
    }

    /**
     * Create a hash for the given password
     *
     * @param string $password
     * @return string
     */
    protected function createPasswordHash(string $password)
    {
        return $this->encryptor->getHash($password, true);
    }

    /**
     * {@inheritdoc}
     */
    public function getSpaazaUserInfoByCustomerId(int $customerId)
    {
        $customer = $this->customerRepository->getById($customerId);
        $spaazaData = $this->customerConnector->getSpaazaUserInfoForCustomer($customer, true);
        return $this->userInfoInterfaceFactory->create(['data' => $spaazaData]);
    }

    /**
     * {@inheritdoc}
     */
    public function getSpaazaUserInfoByCustomerEmail(string $customerEmail)
    {
        $customer = $this->customerRepository->get($customerEmail);
        $spaazaData = $this->customerConnector->getSpaazaUserInfoForCustomer($customer, true);
        return $this->userInfoInterfaceFactory->create(['data' => $spaazaData]);
    }
}
