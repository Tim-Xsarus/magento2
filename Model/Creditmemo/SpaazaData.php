<?php

namespace Spaaza\Loyalty\Model\Creditmemo;

use Magento\Framework\Model\AbstractModel;
use Spaaza\Loyalty\Api\Data\Creditmemo\SpaazaDataInterface;

/**
 * @method \Spaaza\Loyalty\Model\ResourceModel\Creditmemo\SpaazaData getResource()
 * @method \Spaaza\Loyalty\Model\ResourceModel\Creditmemo\SpaazaData\Collection getCollection()
 */
class SpaazaData extends AbstractModel implements SpaazaDataInterface
{

    protected function _construct()
    {
        $this->_init(\Spaaza\Loyalty\Model\ResourceModel\Creditmemo\SpaazaData::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getCreditmemoId(): ?int
    {
        return $this->getData(self::KEY_CREDITMEMO_ID) !== null
            ? (int)$this->getData(self::KEY_CREDITMEMO_ID)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreditmemoId(?int $creditmemoId)
    {
        return $this->setData(self::KEY_CREDITMEMO_ID, $creditmemoId);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherAmount(): ?float
    {
        return $this->getData(self::KEY_VOUCHER_AMOUNT) !== null
            ? (float)$this->getData(self::KEY_VOUCHER_AMOUNT)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherAmount(float $voucherAmount)
    {
        return $this->setData(self::KEY_VOUCHER_AMOUNT, $voucherAmount);
    }

    /**
     * {@inheritdoc}
     */
    public function getBaseVoucherAmount(): ?float
    {
        return $this->getData(self::KEY_BASE_VOUCHER_AMOUNT) !== null
            ? (float)$this->getData(self::KEY_BASE_VOUCHER_AMOUNT)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setBaseVoucherAmount(float $baseVoucherAmount)
    {
        return $this->setData(self::KEY_BASE_VOUCHER_AMOUNT, $baseVoucherAmount);
    }

    /**
     * {@inheritdoc}
     */
    public function getBasketId(): ?int
    {
        return $this->getData(self::KEY_BASKET_ID) !== null
            ? (int)$this->getData(self::KEY_BASKET_ID)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setBasketId(?int $basketId)
    {
        return $this->setData(self::KEY_BASKET_ID, $basketId);
    }
}
