<?php

namespace Spaaza\Loyalty\Model\Connector;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Exception\InputException;
use Spaaza\Loyalty\Model\Config;
use Spaaza\Loyalty\Api\Data\Client\RequestInterface;
use Spaaza\Loyalty\Model\Config\Source\EntityType;
use Magento\Sales\Api\Data\OrderInterface;

class Voucher
{
    const VOUCHER_LOCK_PERIOD = 600;

    /**
     * @var \Spaaza\Loyalty\Model\Client\Request\Queue
     */
    private $requestQueue;

    /**
     * @var \Spaaza\Loyalty\Api\Data\Client\RequestInterfaceFactory
     */
    private $requestFactory;

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    private $helper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var Registry
     */
    private $connectorRegistry;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement
     */
    private $customerSpaazaDataManagement;

    public function __construct(
        \Spaaza\Loyalty\Model\Client\Request\Queue $requestQueue,
        \Spaaza\Loyalty\Api\Data\Client\RequestInterfaceFactory $requestFactory,
        Registry $connectorRegistry,
        \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement $customerSpaazaDataManagement,
        Config $config,
        \Spaaza\Loyalty\Helper\Data $helper,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->requestQueue = $requestQueue;
        $this->requestFactory = $requestFactory;
        $this->helper = $helper;
        $this->logger = $logger;
        $this->connectorRegistry = $connectorRegistry;
        $this->config = $config;
        $this->customerSpaazaDataManagement = $customerSpaazaDataManagement;
    }

    /**
     * Claim a voucher (synchronous)
     *
     * @param string $voucherKey
     * @param CustomerInterface $customer
     * @return array  The json decoded API response
     * @throws InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function claimVoucher($voucherKey, CustomerInterface $customer)
    {
        $spaazaData = $this->customerSpaazaDataManagement->applyExtensionAttributes($customer);
        if (!$spaazaData->getUserId()) {
            throw new InputException(__('Cannot do voucher actions for a non-Spaaza user'));
        }

        /** @var RequestInterface $request */
        $request = $this->requestFactory->create();
        $request
            ->setPath('claim-voucher.json')
            ->setPayload(
                [
                    'voucher_key' => $voucherKey,
                    'user_id' => $spaazaData->getUserId(),
                ]
            )
            ->setMethod(RequestInterface::METHOD_POST)
            ->setEntityType(EntityType::ENTITY_TYPE_CUSTOMER)
            ->setEntityId($customer->getId())
            ->setOptions(['send_chain_id' => true, 'send_hostname' => true]);

        $result = $this->requestQueue->sendSynchronousRequest($request, true);
        $this->connectorRegistry->deleteCard($customer->getId());
        return $result;
    }

    /**
     * Unclaim a voucher (synchronous)
     *
     * @param string $voucherKey
     * @param CustomerInterface $customer
     * @return array  The json decoded API response
     * @throws InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function unclaimVoucher($voucherKey, CustomerInterface $customer)
    {
        $spaazaData = $this->customerSpaazaDataManagement->applyExtensionAttributes($customer);
        if (!$spaazaData->getUserId()) {
            throw new InputException(__('Cannot do voucher actions for a non-Spaaza user'));
        }

        /** @var RequestInterface $request */
        $request = $this->requestFactory->create();
        $request
            ->setPath('unclaim-voucher.json')
            ->setPayload(
                [
                    'voucher_key' => $voucherKey,
                    'user_id' => $spaazaData->getUserId(),
                ]
            )
            ->setEntityType(EntityType::ENTITY_TYPE_CUSTOMER)
            ->setEntityId($customer->getId())
            ->setMethod(RequestInterface::METHOD_POST)
            ->setOptions(['send_chain_id' => true, 'send_hostname' => true]);

        $result = $this->requestQueue->sendSynchronousRequest($request, true);
        $this->connectorRegistry->deleteCard($customer->getId());
        return $result;
    }

    /**
     * Lock a voucher (asynchronous if needed)
     *
     * @param string $voucherKey
     * @param int $customerId
     * @param OrderInterface|null $order
     * @return void
     */
    public function lockVoucher($voucherKey, $customerId, OrderInterface $order = null)
    {
        $payload = [
            'voucher_key' => $voucherKey,
            'voucher_lock_period' => $this->config->getVoucherLockPeriod(),
        ];
        if ($order && $order->getIncrementId()) {
            $payload['retailer_basket_code'] = $order->getIncrementId();
        }

        /** @var RequestInterface $request */
        $request = $this->requestFactory->create();
        $request
            ->setPath('lock-voucher.json')
            ->setPayload($payload)
            ->setEntityType(EntityType::ENTITY_TYPE_CUSTOMER)
            ->setEntityId($customerId)
            ->setMethod(RequestInterface::METHOD_PUT)
            ->setOptions(['send_chain_id' => true, 'send_hostname' => true]);

        $this->requestQueue->addRequest($request, true);
        $this->connectorRegistry->deleteCard($customerId);
    }
}
