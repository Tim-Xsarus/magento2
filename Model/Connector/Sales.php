<?php

namespace Spaaza\Loyalty\Model\Connector;

use Magento\Sales\Api\Data\CreditmemoInterface;
use Magento\Sales\Api\Data\CreditmemoItemInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Spaaza\Loyalty\Api\Data\Client\RequestInterface;
use Spaaza\Loyalty\Api\Data\VoucherInterface;
use Spaaza\Loyalty\Model\Config\Source\EntityType;

class Sales
{
    const DATE_TIME_FORMAT_ISO8601 = 'Y-m-d\TH:i:sO';

    /**
     * @var \Spaaza\Loyalty\Model\Order\SpaazaDataManagement
     */
    private $spaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement
     */
    private $customerSpaazaDataManagement;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    private $eventManager;

    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    private $config;

    /**
     * @var \Spaaza\Loyalty\Helper\Catalog
     */
    private $catalogHelper;

    /**
     * @var \Spaaza\Loyalty\Helper\Sales
     */
    private $salesHelper;

    /**
     * @var \Magento\Tax\Helper\Data
     */
    private $taxHelper;

    /**
     * @var \Spaaza\Loyalty\Api\Data\Client\RequestInterfaceFactory
     */
    private $requestFactory;

    /**
     * @var \Spaaza\Loyalty\Model\Client\Request\Queue
     */
    private $requestQueue;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    private $localeDate;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    public function __construct(
        \Spaaza\Loyalty\Model\Order\SpaazaDataManagement $spaazaDataManagement,
        \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement $customerSpaazaDataManagement,
        \Spaaza\Loyalty\Model\Config $config,
        \Spaaza\Loyalty\Helper\Catalog $catalogHelper,
        \Spaaza\Loyalty\Helper\Sales $salesHelper,
        \Spaaza\Loyalty\Model\Client\Request\Queue $requestQueue,
        \Spaaza\Loyalty\Api\Data\Client\RequestInterfaceFactory $requestFactory,
        \Magento\Tax\Helper\Data $taxHelper,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->customerSpaazaDataManagement = $customerSpaazaDataManagement;
        $this->eventManager = $eventManager;
        $this->config = $config;
        $this->catalogHelper = $catalogHelper;
        $this->requestQueue = $requestQueue;
        $this->requestFactory = $requestFactory;
        $this->salesHelper = $salesHelper;
        $this->taxHelper = $taxHelper;
        $this->localeDate = $localeDate;
        $this->logger = $logger;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Send an order to Spaaza (asynchronous if needed)
     *
     * This is a separate function to be able to call it separately
     *
     * @param OrderInterface $order
     * @param bool $trySynchronous  First try synchronous before falling back to asynchronous call?
     * @return void
     */
    public function sendOrder(OrderInterface $order, $trySynchronous = true)
    {
        try {
            $request = $this->createOrderRequest($order);
            if ($request) {
                $this->requestQueue->addRequest($request, $trySynchronous);
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
        }
    }

    /**
     * Create a request object for an order
     *
     * @param OrderInterface $order
     * @return null|RequestInterface
     */
    public function createOrderRequest(OrderInterface $order)
    {
        $request = null;
        $spaazaData = $this->spaazaDataManagement->applyExtensionAttributes($order);

        if ($spaazaData->getBasketId()) {
            // don't send orders twice
            return null;
        }

        $userIdentity = $this->getUserIdentityForOrder($order);
        if (!$userIdentity) {
            // user cannot be identified: skip this order
            return null;
        }

        $basketItems = [];

        foreach ($order->getItems() as $item) {
            if (!$item->getParentItemId()) {
                // This is a 'visible item' and should always hold the price and qty
                // In the case of a configurable item, this has the SKU of the simple and the product id of the
                // configurable product
                $price = $item->getPriceInclTax();
                $qty = $item->getQtyOrdered();
                $discount = $item->getDiscountAmount() / $item->getQtyOrdered();

                $salePrice = $price - $discount;
                $transport = new \Magento\Framework\DataObject([
                    'product_sale_price' => (float)$salePrice,
                    'product_quantity' => (float)$qty,
                ]);
                $this->eventManager->dispatch('spaaza_order_product_sale_price', [
                    'order_item' => $item,
                    'order' => $order,
                    'transport' => $transport,
                ]);
                $basketItem = $this->catalogHelper->getBasketItemIdentification($item);
                $basketItem['retailer_item_code'] = (string)$item->getItemId();
                $basketItem['item_quantity'] = (float)$transport->getData('product_quantity');
                $basketItem['item_price'] = (float)$transport->getData('product_sale_price');

                $basketItems[] = $basketItem;
            }
        }

        $storeTimezone = $this->localeDate->getConfigTimezone('store', $order->getStoreId());
        // This produces the datetime in UTC
        $dateTime = new \DateTime($order->getCreatedAt());
        // Now move it to the store's timezone
        $dateTime->setTimezone(new \DateTimeZone($storeTimezone));

        $payload = [
            'entity' => [
                'entity_type' => 'chain', // 'chain' or 'business'
                'entity_id' => $this->config->getChainId(),
            ],
            'basket' => [
                'app_platform_type' => 'online', // 'in_store' or 'online'
                'basket_timestamp_iso8601' => $dateTime->format(self::DATE_TIME_FORMAT_ISO8601),
                'basket_timezone_name' => $storeTimezone,
                // any ID in the retailer's system used to identify a particular user basket.
                'retailer_basket_code' => $this->salesHelper->getOrderOwnerCode($order),
                'basket_total_price' => (float)$order->getGrandTotal() - (float)$order->getShippingInclTax(),
                'shipping_charge' => (float)$order->getShippingInclTax(),
                'basket_currency' => [
                    'currency_code' => $order->getBaseCurrencyCode() // 3 letter currency code
                ],
                'basket_tax' => $this->getBasketTaxForOrder($order),
                'basket_items' => $basketItems,
            ]
        ];

        // redeem standalone vouchers
        $claimedVouchers = $spaazaData->getVouchers();
        $basketVouchers = [];
        $honourVouchers = [];
        if (is_array($claimedVouchers)) {
            foreach ($claimedVouchers as $voucherInfo) {
                if (!$voucherInfo->getType() || $voucherInfo->getType() == VoucherInterface::VOUCHER_TYPE_BASKET) {
                    $basketVouchers[] = ['voucher_key' => $voucherInfo->getKey()];
                } elseif ($voucherInfo->getType() == VoucherInterface::VOUCHER_TYPE_HONOUR) {
                    $honourVouchers[] = ['voucher_key' => $voucherInfo->getKey()];
                }
            }
        }
        $payload['basket']['basket_vouchers'] = $basketVouchers;
        $payload['basket']['honour_vouchers'] = $honourVouchers;

        $payload['user'] = $userIdentity;

        $transport = new \Magento\Framework\DataObject([
            'payload' => $payload,
        ]);
        $this->eventManager->dispatch('spaaza_order_request_before', [
            'order' => $order,
            'transport' => $transport,
        ]);
        $payload = $transport->getData('payload');

        if ($payload) {
            $request = $this->requestFactory->create()
                ->setPath('auth/add-basket.json')
                ->setPayload($payload)
                ->setEntityType(EntityType::ENTITY_TYPE_ORDER)
                ->setEntityId($order->getEntityId())
                ->setMethod(RequestInterface::METHOD_POST_JSON)
                ->setCallback('orderSaveBasketId')
                ->setOptions(
                    [
                        'api_version' => '1.1.8',
                    ]
                );
        }
        return $request;
    }

    /**
     * Get the user identity part for use in an add-basket call
     *
     * @param OrderInterface $order
     * @return array
     */
    private function getUserIdentityForOrder(OrderInterface $order)
    {
        $payload = [];
        $spaazaUserId = null;
        $spaazaMemberNumber = null;
        $customer = null;

        $spaazaData = $order->getExtensionAttributes()->getSpaazaData();
        if ($spaazaData->getUserId()) {
            $spaazaUserId = $spaazaData->getUserId();
        } elseif ($spaazaData->getMemberNumber()) {
            $spaazaMemberNumber = $spaazaData->getMemberNumber();
        } elseif ($order->getCustomerId()) {
            $customerSpaazaData = $this->customerSpaazaDataManagement->getByCustomerId($order->getCustomerId());
            $spaazaUserId = $customerSpaazaData->getUserId();
        }

        if (!$spaazaUserId && !$spaazaMemberNumber) {
            // We have no Spaaza user to assign this order to
            return $payload;
        }

        if ($spaazaUserId) {
            $payload = [
                'member_programme' => 'spaaza',
                'spaaza_user_id' => (int)$spaazaUserId
            ];
        } elseif ($spaazaMemberNumber) {
            $payload = [
                'member_programme' => 'spaaza',
                'member_number' => $spaazaMemberNumber
            ];
        }

        return $payload;
    }

    /**
     * Get the basket tax rates
     *
     * @param OrderInterface $order
     * @return array  Array ready to pass to Spaaza
     */
    protected function getBasketTaxForOrder(OrderInterface $order)
    {
        $taxItems = $this->taxHelper->getCalculatedTaxes($order);
        $result = [];
        foreach ($taxItems as $taxItem) {
            $result[] = [
                'tax_rate' => round($taxItem['percent'] / 100, 2),
                'tax_total' => round($taxItem['tax_amount'], 2), // base_tax_amount is also available
            ];
        }
        return $result;
    }

    /**
     * Send an order to Spaaza (asynchronous if needed)
     *
     * This is a separate function to be able to call it separately
     *
     * @param CreditmemoInterface $creditmemo
     * @param bool $trySynchronous First try synchronous before falling back to asynchronous call?
     * @return void
     */
    public function sendCreditmemo(CreditmemoInterface $creditmemo, $trySynchronous = true)
    {
        try {
            $request = $this->createCreditmemoRequest($creditmemo);
            if ($request) {
                $this->requestQueue->addRequest($request, $trySynchronous);
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
        }
    }

    /**
     * Send a credit memo to Spaaza (asynchronous if needed)
     *
     * @param CreditmemoInterface $creditmemo
     * @return RequestInterface|null
     */
    public function createCreditmemoRequest(CreditmemoInterface $creditmemo)
    {
        $order = $this->orderRepository->get($creditmemo->getOrderId());

        $request = null;
        $spaazaBasketId = $order->getExtensionAttributes()->getSpaazaData()->getBasketId();
        if (!$spaazaBasketId) {
            // apparently this order has not been sent to spaaza
            return null;
        }

        $userIdentity = $this->getUserIdentityForOrder($order);
        if (!$userIdentity) {
            // if the user cannot be identified: don't send the creditmemo
            return null;
        }

        $basketItems = [];
        foreach ($creditmemo->getItems() as $creditmemoItem) {
            $orderItem = $this->getOrderItemForCreditmemoItem($creditmemoItem, $order);
            if (!$orderItem || $orderItem->getParentItemId()) {
                // skip child items
                continue;
            }

            $qty = (float)$creditmemoItem->getQty();
            $itemIdentification = $this->catalogHelper->getBasketItemIdentificationForCreditmemoItem($creditmemoItem);
            if ($itemIdentification && $qty > 0) {
                $price = $creditmemoItem->getPriceInclTax();
                $discount = $creditmemoItem->getDiscountAmount() / $qty;

                $salePrice = -1 * ($price - $discount);
                $transport = new \Magento\Framework\DataObject([
                    'product_sale_price' => (float)$salePrice,
                    'product_quantity' => (float)$qty,
                ]);
                $this->eventManager->dispatch('spaaza_order_product_sale_price', [
                    'order_item' => $orderItem,
                    'creditmemo_item' => $creditmemoItem,
                    'return_item' => true,
                    'order' => $order,
                    'creditmemo' => $creditmemo,
                    'transport' => $transport,
                ]);
                $basketItem = $itemIdentification;
                $basketItem['item_quantity'] = (float)$transport->getProductQuantity();
                $basketItem['item_price'] = (float)$transport->getProductSalePrice();
                $basketItem['return_item'] = true;
                $basketItem['original_retailer_basket_code'] = $this->salesHelper->getOrderOwnerCode($order);
                $basketItems[] = $basketItem;
            }
        }

        if (count($basketItems) == 0) {
            return null;
        }

        $storeTimezone = $this->localeDate->getConfigTimezone('store', $order->getStoreId());
        // This produces the datetime in UTC
        $dateTime = new \DateTime($order->getCreatedAt());
        // Now move it to the store's timezone
        $dateTime->setTimezone(new \DateTimeZone($storeTimezone));

        $payload = [
            'entity' => [
                'entity_type' => 'chain', // 'chain' or 'business'
                'entity_id' => $this->config->getChainId(),
            ],
            'user' => $userIdentity,
            'basket' => [
                'app_platform_type' => 'online', // 'in_store' or 'online'
                'basket_timestamp_iso8601' => $dateTime->format(self::DATE_TIME_FORMAT_ISO8601),
                // any ID in the retailer's system used to identify a particular user basket.
                'retailer_basket_code' => $this->salesHelper->getCreditmemoOwnerCode($creditmemo),
                'basket_total_price' => -1.0 * ($creditmemo->getGrandTotal() - $creditmemo->getShippingInclTax()),
                'shipping_charge' => -1.0 * $creditmemo->getShippingInclTax(),
                'basket_currency' => [
                    'currency_code' => $creditmemo->getBaseCurrencyCode() // 3 letter currency code
                ],
                'basket_tax' => $this->getBasketTaxForCreditmemo($creditmemo, $order),
                'basket_items' => $basketItems,
            ],
        ];

        $transport = new \Magento\Framework\DataObject([
            'payload' => $payload,
        ]);
        $this->eventManager->dispatch('spaaza_creditmemo_request_before', [
            'creditmemo' => $creditmemo,
            'transport' => $transport,
        ]);
        $payload = $transport->getData('payload');

        if ($payload) {
            $request = $this->requestFactory->create()
                ->setPath('auth/add-basket.json')
                ->setPayload($payload)
                ->setEntityType(EntityType::ENTITY_TYPE_CREDITMEMO)
                ->setEntityId($creditmemo->getEntityId())
                ->setMethod(RequestInterface::METHOD_POST_JSON)
                ->setCallback('creditmemoSaveBasketId')
                ->setOptions(
                    [
                        'api_version' => '1.1.8',
                    ]
                );
        }
        return $request;
    }

    /**
     * Get basket tax rates for a credit memo
     *
     * Unfortunately, there's no easy way like for orders. We need to compose the values ourselves.
     *
     * @param CreditmemoInterface $creditmemo The creditmemo to calculate tax for
     * @param OrderInterface $order
     * @return array  Array ready to pass to Spaaza (with negative values)
     */
    private function getBasketTaxForCreditmemo(CreditmemoInterface $creditmemo, OrderInterface $order)
    {
        $taxrates = [];
        foreach ($creditmemo->getItems() as $creditmemoItem) {
            if ($orderItem = $this->getOrderItemForCreditmemoItem($creditmemoItem, $order)) {
                $percent = $orderItem->getTaxPercent();
                if (!isset($taxrates[$percent])) {
                    $taxrates[$percent] = [
                        'tax_rate' => round($percent / 100, 2),
                        'tax_total' => 0,
                    ];
                }
                $taxrates[$percent]['tax_total'] -= $creditmemoItem->getTaxAmount();
            }
        }
        return array_values($taxrates);
    }

    /**
     * Get the corresponding order item for a credit memo item
     *
     * @param CreditmemoItemInterface $creditmemoItem
     * @param OrderInterface $order
     * @return OrderItemInterface|null
     */
    private function getOrderItemForCreditmemoItem(CreditmemoItemInterface $creditmemoItem, OrderInterface $order)
    {
        $orderItem = null;
        foreach ($order->getItems() as $_orderItem) {
            if ($_orderItem->getItemId() == $creditmemoItem->getOrderItemId()) {
                $orderItem = $_orderItem;
                break;
            }
        }
        return $orderItem;
    }
}
