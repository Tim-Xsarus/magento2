<?php

namespace Spaaza\Loyalty\Model\Client\Request;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Notification\MessageInterface;
use Spaaza\Loyalty\Model\Client\Request;
use Spaaza\Loyalty\Model\Config\Source\EntityType;

class CallbackProcessor
{

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Spaaza\Loyalty\Model\Order\SpaazaDataManagement
     */
    private $orderSpaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    private $helper;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement
     */
    private $customerSpaazaDataManagement;

    /**
     * @var \Magento\Sales\Api\CreditmemoRepositoryInterface
     */
    private $creditmemoRepository;

    /**
     * @var \Spaaza\Loyalty\Model\Creditmemo\SpaazaDataManagement
     */
    private $creditmemoSpaazaDataManagement;

    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Api\CreditmemoRepositoryInterface $creditmemoRepository,
        \Spaaza\Loyalty\Model\Order\SpaazaDataManagement $orderSpaazaDataManagement,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement $customerSpaazaDataManagement,
        \Spaaza\Loyalty\Model\Creditmemo\SpaazaDataManagement $creditmemoSpaazaDataManagement,
        \Spaaza\Loyalty\Helper\Data $helper,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->orderRepository = $orderRepository;
        $this->logger = $logger;
        $this->orderSpaazaDataManagement = $orderSpaazaDataManagement;
        $this->helper = $helper;
        $this->customerRepository = $customerRepository;
        $this->customerSpaazaDataManagement = $customerSpaazaDataManagement;
        $this->creditmemoRepository = $creditmemoRepository;
        $this->creditmemoSpaazaDataManagement = $creditmemoSpaazaDataManagement;
    }

    /**
     * Save a basket id after an order has been sent to Spaaza
     *
     * @param Request $request
     * @param array $response  The json-decoded response of the Spaaza API
     * @return void
     */
    public function orderSaveBasketId(Request $request, $response)
    {
        $this->needEntityType($request, EntityType::ENTITY_TYPE_ORDER);
        $orderId = $request->getEntityId();
        if (empty(($response['basket']['id']))) {
            return;
        }
        $basketId = $response['basket']['id'];

        try {
            $order = $this->orderRepository->get($orderId);

            $spaazaData = $order->getExtensionAttributes()->getSpaazaData();
            $spaazaData->setBasketId($basketId);

            $this->orderSpaazaDataManagement->save($spaazaData);

            $sentVouchers = $spaazaData->getVouchers();
            if (!$sentVouchers) {
                return;
            }

            // check the applied vouchers
            $sentVoucherKeys = [];
            foreach ($sentVouchers as $voucherInfo) {
                $sentVoucherKeys[] = $voucherInfo->getKey();
            }
            $appliedVouchers = isset($response['basket']['basket_vouchers_applied'])
                ? $response['basket']['basket_vouchers_applied']
                : [];
            $appliedVoucherKeys = [];
            foreach ((array)$appliedVouchers as $appliedVoucher) {
                if (isset($appliedVoucher['voucher_key'])) {
                    $appliedVoucherKeys[] = $appliedVoucher['voucher_key'];
                }
            }

            $diff = array_diff($sentVoucherKeys, $appliedVoucherKeys);
            if ($diff) {
                $message = __(
                    'The voucher(s) %s have been sent to Spaaza, but were not applied to the order by Spaaza. This most probably means that the voucher(s) have been spent twice.',
                    implode(', ', $diff)
                );

                $this->helper->publishError(
                    'vouchers_not_applied',
                    __('Vouchers have not been applied to order.'),
                    $message,
                    MessageInterface::SEVERITY_MAJOR,
                    [
                        'vouchers_sent' => $sentVoucherKeys,
                        'vouchers_applied' => $appliedVoucherKeys,
                        'order' => $order,
                    ]
                );
            }

            // Save the voucher distribution
            $basketItems = !empty($response['basket']['basket_items'])
                ? $response['basket']['basket_items']
                : [];
            $voucherAmounts = [];
            foreach ($basketItems as $basketItem) {
                if (!empty($basketItem['basket_voucher_distribution']) && !empty($basketItem['item_barcode'])) {
                    if (!isset($voucherAmounts[$basketItem['item_barcode']])) {
                        $voucherAmounts[$basketItem['item_barcode']] = [
                            'item_barcode' => $basketItem['item_barcode'],
                            'amount_total' => 0,
                            'qty' => 0,
                        ];
                    }
                    foreach ($basketItem['basket_voucher_distribution'] as $distribution) {
                        if (!empty($distribution['voucher_distribution_amount'])) {
                            $voucherAmounts[$basketItem['item_barcode']]['amount_total']
                                += $distribution['voucher_distribution_amount'];
                        }
                    }
                    $voucherAmounts[$basketItem['item_barcode']]['qty'] += $basketItem['item_quantity'];
                }
            }

            $spaazaData->setVoucherDistribution(array_values($voucherAmounts));
        } catch (NoSuchEntityException $e) {
            $this->logger->error($e);
        } finally {
            if (isset($spaazaData)) {
                $this->orderSpaazaDataManagement->save($spaazaData);
            }
        }
    }

    /**
     * Save a basket id after a credit memo has been sent to Spaaza
     *
     * @param Request $request
     * @param $response
     * @return void
     */
    public function creditmemoSaveBasketId(Request $request, $response)
    {
        $this->needEntityType($request, EntityType::ENTITY_TYPE_CREDITMEMO);
        $creditmemoId = $request->getEntityId();
        if (!empty(($response['basket']['id']))) {
            $basketId = $response['basket']['id'];

            try {
                $creditmemo = $this->creditmemoRepository->get($creditmemoId);

                $spaazaData = $creditmemo->getExtensionAttributes()->getSpaazaData();
                $spaazaData->setBasketId($basketId);

                $this->creditmemoSpaazaDataManagement->save($spaazaData);
            } catch (NoSuchEntityException $e) {
                $this->logger->error($e);
            }
        }
    }

    /**
     * Save the Spaaza user id and member_number after creating or updating a user
     *
     * @param Request $request
     * @param array $response
     * @return void
     */
    public function customerSetSpaazaIdentification(Request $request, $response)
    {
        $this->needEntityType($request, EntityType::ENTITY_TYPE_CUSTOMER);

        if (!empty(($response['user_info']['id']))) {
            $spaazaUserId = $response['user_info']['id'];
            $customerId = $request->getEntityId();

            try {
                $customer = $this->customerRepository->getById($customerId);
                $spaazaData = $this->customerSpaazaDataManagement->applyExtensionAttributes($customer);
                $spaazaData->setUserId($response['user_info']['id']);
                $spaazaData->setMemberNumber($response['entity_code']['code'] ?? null);
                $this->customerSpaazaDataManagement->save($spaazaData);
            } catch (NoSuchEntityException $e) {
                $this->logger->error($e);
            }
        }
    }

    /**
     * Make sure that the given request is assigned to an entity of a certain type
     *
     * Throws an exception if the criterium is not met
     *
     * @throws \Exception
     * @param Request $request
     * @param string $entityType  One of Spaaza_Myprice_Model_Api_Queue_Request::ENTITY_TYPE_XXX constants
     *                            (which are equal to eav_entity_type codes)
     * @return void
     */
    protected function needEntityType(Request $request, $entityType)
    {
        if ($request->getEntityType() != $entityType) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Callback needs a request for entity type %s, but the request has entity type %s',
                    $entityType,
                    $request->getEntityType()
                )
            );
        }
    }
}
