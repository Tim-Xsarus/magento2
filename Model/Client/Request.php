<?php

namespace Spaaza\Loyalty\Model\Client;

use Spaaza\Loyalty\Api\Data\Client\RequestInterface;

/**
 * @method \Spaaza\Loyalty\Model\ResourceModel\Client\Request getResource()
 * @method \Spaaza\Loyalty\Model\ResourceModel\Client\Request\Collection getCollection()
 */
class Request extends \Magento\Framework\Model\AbstractModel implements RequestInterface
{
    protected $_eventPrefix = 'spaaza_client_request';
    protected $_eventObject = 'request';

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $dateTime;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    protected $helper;

    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    private $config;

    /**
     * @var bool
     */
    protected $hasErrorBeenLogged = false;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Spaaza\Loyalty\Helper\Data $helper,
        \Spaaza\Loyalty\Model\Config $config,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->dateTime = $dateTime;
        $this->scopeConfig = $scopeConfig;
        $this->helper = $helper;
        $this->config = $config;
    }

    protected function _construct()
    {
        $this->setMethod(self::METHOD_GET);
        $this->setStatus(self::STATUS_PENDING);
        $this->_init(\Spaaza\Loyalty\Model\ResourceModel\Client\Request::class);
    }

    /**
     * Mark the request sent
     *
     * @return $this
     */
    public function markSent()
    {
        return $this->setSentAt($this->dateTime->gmtDate());
    }

    /**
     * Mark the request as complete
     *
     * @return $this
     */
    public function markComplete()
    {
        return $this->setStatus(self::STATUS_COMPLETE);
    }

    /**
     * Mark the request as error, along with a message
     *
     * @param null|string $message
     * @return $this
     */
    public function markError($message = null)
    {
        if ($message) {
            $this->setMessage($message);
        }
        return $this->setStatus(self::STATUS_ERROR);
    }

    /**
     * Mark the request as processing (and increase the attempts)
     *
     * @return $this
     */
    public function markProcessing()
    {
        $this->setData('attempts', $this->getOrigData('attempts') + 1);
        return $this->setStatus(self::STATUS_PROCESSING);
    }

    /**
     * Mark the request as pending (do the request later)
     *
     * @return $this
     */
    public function markPending()
    {
        return $this->setStatus(self::STATUS_PENDING);
    }

    /**
     * Retry the request if it was in error; it resets the attempts
     *
     * @return $this
     */
    public function retry()
    {
        return $this->markPending()
            ->setAttempts(0);
    }

    /**
     * Mark the request as failed if the maximum number of attempts has been exceeded
     *
     * @return $this
     */
    public function beforeSave()
    {
        $maxAttempts = $this->config->getRequestMaxAttempts();
        if ($this->getAttempts() > $maxAttempts
            && $this->getStatus() != self::STATUS_COMPLETE
            && $this->getStatus() != self::STATUS_ERROR
        ) {
            $this->markError(sprintf('Max attempts of %d has been exceeded', $maxAttempts));
        }
        return parent::beforeSave();
    }

    public function afterSave()
    {
        if ($this->getOrigData('status') != $this->getStatus() && $this->getStatus() == self::STATUS_ERROR) {
            if (!$this->getHasErrorBeenLogged()) {
                // synchronous calls are not important enough to add notifications about
                if (!$this->getIsSynchronous()) {
                    $severity = \Magento\Framework\Notification\MessageInterface::SEVERITY_MAJOR;
                    $title = __('Sending request to Spaaza failed');
                    $message = __(
                        'Request #%1 about %2 #%3 has not been sent to Spaaza after %4 tries. We gave up. You could manually reset the request to try again. The last error message was: %5.',
                        (int)$this->getId(),
                        $this->helper->getEntityTypeLabel($this->getEntityType()),
                        (int)$this->getEntityId(),
                        (int)$this->getAttempts(),
                        $this->getMessage()
                    );

                    $this->helper->publishError('spaaza_api_request_error', $title, $message, $severity, [
                        'request' => $this,
                    ]);
                }
                $this->setHasErrorBeenLogged(true);
            }
        }
        return parent::afterSave();
    }

    /**
     * {@inheritdoc}
     */
    public function setPayload($payload)
    {
        if (!is_string($payload)) {
            // if it's no string: try to json encode it
            $payload = json_encode($payload);
        }
        return $this->setData(self::KEY_PAYLOAD, $payload);
    }

    /**
     * {@inheritdoc}
     */
    public function getPayload()
    {
        $data = $this->getData(self::KEY_PAYLOAD);
        return is_string($data) ? json_decode($data, true) : $data;
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions(): array
    {
        $data = $this->getData(self::KEY_OPTIONS);
        return !is_array($data) ? (array)json_decode($data, true) : $data;
    }

    /**
     * {@inheritdoc}
     */
    public function setOptions($options)
    {
        if (!is_string($options)) {
            // if it's no string: try to json encode it
            $options = json_encode($options);
        }
        return $this->setData(self::KEY_OPTIONS, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestId()
    {
        return (int)$this->getData(self::KEY_REQUEST_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setRequestId($requestId)
    {
        return $this->setData(self::KEY_REQUEST_ID, (int)$requestId);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityType()
    {
        return $this->getData(self::KEY_ENTITY_TYPE);
    }

    /**
     * {@inheritdoc}
     */
    public function setEntityType($entityType)
    {
        return $this->setData(self::KEY_ENTITY_TYPE, $entityType);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityId()
    {
        return $this->getData(self::KEY_ENTITY_ID) !== null
            ? (int)$this->getData(self::KEY_ENTITY_ID)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::KEY_ENTITY_ID, $entityId !== null ? (int)$entityId : null);
    }

    /**
     * {@inheritdoc}
     */
    public function getPath()
    {
        return $this->getData(self::KEY_PATH);
    }

    /**
     * {@inheritdoc}
     */
    public function setPath($path)
    {
        return $this->setData(self::KEY_PATH, $path);
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return $this->getData(self::KEY_METHOD);
    }

    /**
     * {@inheritdoc}
     */
    public function setMethod($method)
    {
        return $this->setData(self::KEY_METHOD, $method);
    }

    /**
     * {@inheritdoc}
     */
    public function getIsSynchronous()
    {
        return (bool)$this->getData(self::KEY_IS_SYNCHRONOUS);
    }

    /**
     * {@inheritdoc}
     */
    public function setIsSynchronous($isSynchronous)
    {
        return $this->setData(self::KEY_IS_SYNCHRONOUS, (int)$isSynchronous);
    }

    /**
     * {@inheritdoc}
     */
    public function getStatus()
    {
        return $this->getData(self::KEY_STATUS);
    }

    /**
     * {@inheritdoc}
     */
    public function setStatus($status)
    {
        return $this->setData(self::KEY_STATUS, $status);
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse()
    {
        return $this->getData(self::KEY_RESPONSE);
    }

    /**
     * {@inheritdoc}
     */
    public function setResponse($response)
    {
        return $this->setData(self::KEY_RESPONSE, $response);
    }

    /**
     * {@inheritdoc}
     */
    public function getCallback()
    {
        return $this->getData(self::KEY_CALLBACK);
    }

    /**
     * {@inheritdoc}
     */
    public function setCallback($callback)
    {
        return $this->setData(self::KEY_CALLBACK, $callback);
    }

    /**
     * {@inheritdoc}
     */
    public function getMessage()
    {
        return $this->getData(self::KEY_MESSAGE);
    }

    /**
     * {@inheritdoc}
     */
    public function setMessage($message)
    {
        return $this->setData(self::KEY_MESSAGE, $message);
    }

    /**
     * {@inheritdoc}
     */
    public function getExecutionTime()
    {
        return $this->getData(self::KEY_EXECUTION_TIME) !== null
            ? (int)$this->getData(self::KEY_EXECUTION_TIME)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setExecutionTime(?int $executionTime)
    {
        return $this->setData(self::KEY_EXECUTION_TIME, $executionTime);
    }

    /**
     * {@inheritdoc}
     */
    public function getAttempts()
    {
        return (int)$this->getData(self::KEY_ATTEMPTS);
    }

    /**
     * {@inheritdoc}
     */
    public function setAttempts(int $attempts)
    {
        return $this->setData(self::KEY_ATTEMPTS, $attempts);
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt()
    {
        return $this->getData(self::KEY_CREATED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::KEY_CREATED_AT, $createdAt);
    }

    /**
     * {@inheritdoc}
     */
    public function getSentAt()
    {
        return $this->getData(self::KEY_SENT_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setSentAt($sentAt)
    {
        return $this->setData(self::KEY_SENT_AT, $sentAt);
    }

    /**
     * @return bool
     */
    public function getHasErrorBeenLogged(): bool
    {
        return (bool)$this->hasErrorBeenLogged;
    }

    /**
     * @param bool $hasErrorBeenLogged
     */
    public function setHasErrorBeenLogged(bool $hasErrorBeenLogged): void
    {
        $this->hasErrorBeenLogged = $hasErrorBeenLogged;
    }
}
