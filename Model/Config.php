<?php

namespace Spaaza\Loyalty\Model;

use Magento\Store\Model\ScopeInterface;

class Config
{
    const XML_PATH_SYNCHRONOUS_SYNC_ENABLED = 'spaaza_loyalty/general/synchronous_sync_enabled';
    const XML_PATH_ASYNCHRONOUS_SYNC_ENABLED = 'spaaza_loyalty/general/asynchronous_sync_enabled';
    const XML_PATH_DEBUG_LOG_ENABLED = 'spaaza_loyalty/general/debug_log_enabled';
    const XML_PATH_CHAIN_ID = 'spaaza_loyalty/client/chain_id';
    const XML_PATH_VOUCHERS_TOTAL_SORTORDER = 'sales/totals_sort/total_label';
    const XML_PATH_VOUCHERS_TOTAL_LABEL = 'spaaza_loyalty/vouchers/total_label';
    const XML_PATH_VOUCHERS_TOTAL_SHOW_ZERO = 'spaaza_loyalty/vouchers/total_show_zero';
    const XML_PATH_VOUCHERS_LOCK_PERIOD = 'spaaza_loyalty/vouchers/lock_period_seconds';
    const XML_PATH_QUEUE_MAX_ATTEMPTS = 'spaaza_loyalty/requests_queue/max_attempts';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManager
     */
    private $storeManager;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManager $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * Is synchronous sync enabled?
     *
     * @return bool
     */
    public function isSynchronousSyncEnabled()
    {
        return (bool)$this->scopeConfig->getValue(self::XML_PATH_SYNCHRONOUS_SYNC_ENABLED);
    }

    /**
     * Is asynchronous sync enabled?
     *
     * @return bool
     */
    public function isAsynchronousSyncEnabled()
    {
        return (bool)$this->scopeConfig->getValue(self::XML_PATH_ASYNCHRONOUS_SYNC_ENABLED);
    }

    /**
     * Is sync enabled at all?
     *
     * @return bool
     */
    public function isSyncEnabled()
    {
        return (bool)$this->isAsynchronousSyncEnabled() || $this->isSynchronousSyncEnabled();
    }

    /**
     * Is the debug log enabled?
     *
     * @return bool
     */
    public function isDebugLogEnabled()
    {
        return (bool)$this->scopeConfig->getValue(self::XML_PATH_DEBUG_LOG_ENABLED);
    }

    /**
     * Get the configured chain id
     *
     * @return int
     */
    public function getChainId()
    {
        return (int)$this->scopeConfig->getValue(self::XML_PATH_CHAIN_ID);
    }

    /**
     * Get the sort order to use for displaying the vouchers total
     *
     * @param null|string|bool|int|\Magento\Store\Api\Data\StoreInterface $store
     * @return int
     */
    public function getVoucherTotalSortOrder($store = null)
    {
        return (int)$this->scopeConfig->getValue(
            self::XML_PATH_VOUCHERS_TOTAL_SORTORDER,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get the label to use for the vouchers total
     *
     * @param null|string|bool|int|\Magento\Store\Api\Data\StoreInterface $store
     * @return string
     */
    public function getVoucherTotalLabel($store = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_VOUCHERS_TOTAL_LABEL,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get the maximum attempts for a request in the queue before hitting failed status
     *
     * @return int
     */
    public function getRequestMaxAttempts()
    {
        return max((int)$this->scopeConfig->getValue(self::XML_PATH_QUEUE_MAX_ATTEMPTS), 1);
    }

    /**
     * Show the Vouchers total if it's 0?
     *
     * @param null|string|bool|int|\Magento\Store\Api\Data\StoreInterface $store
     * @return bool
     */
    public function showVoucherZeroTotal($store = null)
    {
        return (bool)$this->scopeConfig->getValue(
            self::XML_PATH_VOUCHERS_TOTAL_SHOW_ZERO,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get the number of seconds to lock a voucher
     *
     * @return int
     */
    public function getVoucherLockPeriod()
    {
        return (int) max($this->scopeConfig->getValue(self::XML_PATH_VOUCHERS_LOCK_PERIOD), 120);
    }
}
