<?php

namespace Spaaza\Loyalty\Model;

class VoucherInfoSerializer
{

    /**
     * @var \Spaaza\Loyalty\Api\Data\VoucherInfoInterfaceFactory
     */
    protected $voucherInfoFactory;

    public function __construct(
        \Spaaza\Loyalty\Api\Data\VoucherInfoInterfaceFactory $voucherInfoFactory
    ) {
        $this->voucherInfoFactory = $voucherInfoFactory;
    }

    /**
     * @param array|string $vouchers
     * @return string
     */
    public function serializeVouchers($vouchers)
    {
        if (is_array($vouchers) && count($vouchers) > 0) {
            $vouchersData = [];
            foreach ($vouchers as $voucher) {
                if ($voucher instanceof \Magento\Framework\Api\AbstractSimpleObject) {
                    $vouchersData[] = $voucher->__toArray();
                } elseif (is_array($voucher)) {
                    // This happens in the _beforeSave() of a resource model. The data has been converted
                    // to an array representation of the object. Just assume that if $voucher is an array,
                    // it's an associative array with the object's data.
                    $vouchersData[] = $voucher;
                }
            }
            return json_encode($vouchersData);
        } elseif (is_string($vouchers)) {
            $vouchersData = json_decode($vouchers, true);
            if (is_array($vouchersData)) {
                return json_encode($vouchersData);
            }
        }
        return json_encode([]);
    }

    /**
     * @param array|string $vouchers
     * @return array
     */
    public function unserializeVouchers($vouchers)
    {
        if (!is_array($vouchers)) {
            $vouchers = json_decode($vouchers, true);
        }
        if (is_array($vouchers) && count($vouchers) > 0) {
            $result = [];
            foreach ($vouchers as $voucher) {
                if (is_array($voucher) && !empty($voucher['key'])) {
                    $result[] = $this->voucherInfoFactory->create([
                        'data' => $voucher
                    ]);
                } elseif ($voucher instanceof \Spaaza\Loyalty\Api\Data\VoucherInfoInterface) {
                    $result[] = $voucher;
                }
            }
            return $result;
        }
        return [];
    }
}
