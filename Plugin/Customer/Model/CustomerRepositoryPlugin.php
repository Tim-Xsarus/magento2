<?php

namespace Spaaza\Loyalty\Plugin\Customer\Model;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Customer\Api\Data\CustomerInterface;

class CustomerRepositoryPlugin
{
    /**
     * @var \Spaaza\Loyalty\Model\Connector\Customer
     */
    private $customerConnector;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    private $config;

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    private $helper;

    /**
     * @var \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement
     */
    private $spaazaDataManagement;

    /**
     * CustomerRepositoryPlugin constructor.
     *
     * @param \Spaaza\Loyalty\Model\Connector\Customer $customerConnector
     * @param \Spaaza\Loyalty\Model\Config $config
     * @param \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement $spaazaDataManagement
     * @param \Spaaza\Loyalty\Helper\Data $helper
     * @param \Psr\Log\LoggerInterface $logger
     * @return void
     */
    public function __construct(
        \Spaaza\Loyalty\Model\Connector\Customer $customerConnector,
        \Spaaza\Loyalty\Model\Config $config,
        \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement $spaazaDataManagement,
        \Spaaza\Loyalty\Helper\Data $helper,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->customerConnector = $customerConnector;
        $this->config = $config;
        $this->logger = $logger;
        $this->helper = $helper;
    }

    /**
     * Add Extension Attribute to a loaded customer
     *
     * @param CustomerRepositoryInterface $subject
     * @param CustomerInterface $customer
     * @return CustomerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterGetById(
        CustomerRepositoryInterface $subject,
        CustomerInterface $customer
    ) {
        $this->spaazaDataManagement->applyExtensionAttributes($customer);
        return $customer;
    }

    /**
     * Add Extension Attribute to a loaded customer
     *
     * @param CustomerRepositoryInterface $subject
     * @param CustomerInterface $customer
     * @return CustomerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterGet(
        CustomerRepositoryInterface $subject,
        CustomerInterface $customer
    ) {
        $this->spaazaDataManagement->applyExtensionAttributes($customer);
        return $customer;
    }

    /**
     * Save Spaaza data extension attribute and sync the customer data to Spaaza
     *
     * @param CustomerRepositoryInterface $subject
     * @param CustomerInterface $result
     * @param CustomerInterface $customer
     * @return CustomerInterface
     */
    public function afterSave(
        CustomerRepositoryInterface $subject,
        CustomerInterface $result,
        CustomerInterface $customer
    ) {
        // To be able to do anything, we need both the Spaaza data AND the id. Neither $result nor
        // $customer has both. Only the first always has an id, only the latter has the extension attribute
        if ($customer->getExtensionAttributes() && $customer->getExtensionAttributes()->getSpaazaData()) {
            $result->getExtensionAttributes()->setSpaazaData($customer->getExtensionAttributes()->getSpaazaData());
        }

        try {
            $this->syncCustomerToSpaaza($result, $customer);
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }

        try {
            $this->saveExtensionAttribute($result);
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }

        return $result;
    }

    /**
     * Persist the Spaaza extension attribute
     *
     * @param CustomerInterface $customer
     * @return void
     * @throws CouldNotSaveException
     */
    protected function saveExtensionAttribute(CustomerInterface $customer)
    {
        $extensionAttributes = $customer->getExtensionAttributes();
        if (!$extensionAttributes) {
            return;
        }

        $spaazaData = $extensionAttributes->getSpaazaData();
        if (!$spaazaData) {
            return;
        }

        try {
            $spaazaData->setCustomerId($customer->getId());
            $this->spaazaDataManagement->save($spaazaData);
            $this->helper->debugLog('Saved Spaaza data for customer', ['customer' => $customer->getId()]);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(
                __('Could not save Spaaza data for customer %1: %2', $customer->getId(), $e->getMessage()),
                $e
            );
        }
    }

    /**
     * Sync a customer to Spaaza
     *
     * @param CustomerInterface $result
     * @param CustomerInterface $customer
     * @return void
     */
    protected function syncCustomerToSpaaza(CustomerInterface $result, CustomerInterface $customer)
    {
        $doNotSaveFlag = $this->spaazaDataManagement->getDoNotSyncOnSave($customer);
        if (!$doNotSaveFlag && $this->config->isSyncEnabled()) {
            $trySynchronous = $this->config->isSynchronousSyncEnabled();
            $this->helper->debugLog('Update Spaaza user after customer save', ['customer' => $result->getId()]);
            $this->customerConnector->updateSpaazaUserFromCustomer($result, $trySynchronous, true);
        }
    }
}
