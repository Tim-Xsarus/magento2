<?php

namespace Spaaza\Loyalty\Plugin\Sales\Model;

class QuoteAddressToOrderPlugin
{

    /**
     * @var \Spaaza\Loyalty\Model\QuoteAddress\SpaazaDataManagement
     */
    private $quoteAddressSpaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Model\Order\SpaazaDataManagement
     */
    private $orderSpaazaDataManagement;

    public function __construct(
        \Spaaza\Loyalty\Model\QuoteAddress\SpaazaDataManagement $quoteAddressSpaazaDataManagement,
        \Spaaza\Loyalty\Model\Order\SpaazaDataManagement $orderSpaazaDataManagement
    ) {
        $this->quoteAddressSpaazaDataManagement = $quoteAddressSpaazaDataManagement;
        $this->orderSpaazaDataManagement = $orderSpaazaDataManagement;
    }

    /**
     * Add Spaaza data from a quote address to an order
     *
     * @param \Magento\Quote\Model\Quote\Address\ToOrder $subject
     * @param \Magento\Sales\Api\Data\OrderInterface $resultOrder
     * @param \Magento\Quote\Api\Data\AddressInterface $quoteAddress
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function afterConvert(
        \Magento\Quote\Model\Quote\Address\ToOrder $subject,
        \Magento\Sales\Api\Data\OrderInterface $resultOrder,
        \Magento\Quote\Api\Data\AddressInterface $quoteAddress
    ) {
        $quoteAddressSpaazaData = $this->quoteAddressSpaazaDataManagement->applyExtensionAttributes($quoteAddress);
        $orderSpaazaData = $this->orderSpaazaDataManagement->applyExtensionAttributes($resultOrder);

        $orderSpaazaData->setUserId($quoteAddressSpaazaData->getUserId());
        $orderSpaazaData->setMemberNumber($quoteAddressSpaazaData->getMemberNumber());
        $orderSpaazaData->setBaseVoucherAmount($quoteAddressSpaazaData->getBaseVoucherAmount());
        $orderSpaazaData->setVoucherAmount($quoteAddressSpaazaData->getVoucherAmount());
        $orderSpaazaData->setVouchers($quoteAddressSpaazaData->getVouchers());
        return $resultOrder;
    }
}
