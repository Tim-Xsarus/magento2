<?php

namespace Spaaza\Loyalty\Plugin\Sales\Model;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

/**
 * Class OrderRepositoryPlugin
 *
 * @see OrderRepositoryInterface
 * @package Spaaza\Loyalty
 */
class OrderRepositoryPlugin
{

    /**
     * @var \Spaaza\Loyalty\Model\Connector\Sales
     */
    private $salesConnector;

    /**
     * @var \Spaaza\Loyalty\Model\Order\SpaazaDataManagement
     */
    private $spaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    private $helper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * OrderRepositoryPlugin constructor.
     *
     * @param \Spaaza\Loyalty\Model\Connector\Sales $salesConnector
     * @param \Spaaza\Loyalty\Model\Order\SpaazaDataManagement $spaazaDataManagement
     * @param \Spaaza\Loyalty\Helper\Data $helper
     * @param \Psr\Log\LoggerInterface $logger
     * @return void
     */
    public function __construct(
        \Spaaza\Loyalty\Model\Connector\Sales $salesConnector,
        \Spaaza\Loyalty\Model\Order\SpaazaDataManagement $spaazaDataManagement,
        \Spaaza\Loyalty\Helper\Data $helper,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->salesConnector = $salesConnector;
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->helper = $helper;
        $this->logger = $logger;
    }

    /**
     * Add SpaazaData extension attribute to a loaded order
     *
     * @param OrderRepositoryInterface $subject
     * @param OrderInterface $order
     * @return OrderInterface
     */
    public function afterGet(
        OrderRepositoryInterface $subject,
        OrderInterface $order
    ) {
        $this->spaazaDataManagement->applyExtensionAttributes($order);
        return $order;
    }

    /**
     * Save Spaaza data extension attribute and sync the customer data to Spaaza
     *
     * @param OrderRepositoryInterface $subject
     * @param OrderInterface $order
     * @return OrderInterface
     */
    public function afterSave(
        OrderRepositoryInterface $subject,
        OrderInterface $order
    ) {
        try {
            $this->saveExtensionAttribute($order);
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }

        return $order;
    }

    /**
     * Persist the Spaaza extension attribute
     *
     * @param OrderInterface $order
     * @return void
     * @throws CouldNotSaveException
     */
    protected function saveExtensionAttribute(OrderInterface $order)
    {
        $extensionAttributes = $order->getExtensionAttributes();
        if (!$extensionAttributes) {
            return;
        }

        $spaazaData = $extensionAttributes->getSpaazaData();
        if (!$spaazaData) {
            return;
        }

        try {
            $spaazaData->setOrderId($order->getEntityId());
            $this->spaazaDataManagement->save($spaazaData);
            $this->helper->debugLog('Saved Spaaza data for order', ['order' => $order->getEntityId()]);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(
                __('Could not save Spaaza data for order %1: %2', $order->getEntityId(), $e->getMessage()),
                $e
            );
        }
    }
}
