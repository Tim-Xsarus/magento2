<?php

namespace Spaaza\Loyalty\Plugin\Sales\Model;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Sales\Api\Data\InvoiceInterface;
use Magento\Sales\Api\InvoiceRepositoryInterface;

/**
 * Class InvoiceRepositoryPlugin
 *
 * @see InvoiceRepositoryInterface
 * @package Spaaza\Loyalty
 */
class InvoiceRepositoryPlugin
{

    /**
     * @var \Spaaza\Loyalty\Model\Connector\Sales
     */
    private $salesConnector;

    /**
     * @var \Spaaza\Loyalty\Model\Invoice\SpaazaDataManagement
     */
    private $spaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    private $helper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * OrderRepositoryPlugin constructor.
     *
     * @param \Spaaza\Loyalty\Model\Connector\Sales $salesConnector
     * @param \Spaaza\Loyalty\Model\Invoice\SpaazaDataManagement $spaazaDataManagement
     * @param \Spaaza\Loyalty\Helper\Data $helper
     * @param \Psr\Log\LoggerInterface $logger
     * @return void
     */
    public function __construct(
        \Spaaza\Loyalty\Model\Connector\Sales $salesConnector,
        \Spaaza\Loyalty\Model\Invoice\SpaazaDataManagement $spaazaDataManagement,
        \Spaaza\Loyalty\Helper\Data $helper,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->salesConnector = $salesConnector;
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->helper = $helper;
        $this->logger = $logger;
    }

    /**
     * Add SpaazaData extension attribute to a loaded order
     *
     * @param InvoiceRepositoryInterface $subject
     * @param InvoiceInterface $invoice
     * @return InvoiceInterface
     */
    public function afterGet(
        InvoiceRepositoryInterface $subject,
        InvoiceInterface $invoice
    ) {
        $this->spaazaDataManagement->applyExtensionAttributes($invoice);
        return $invoice;
    }

    /**
     * Save Spaaza data extension attribute and sync the customer data to Spaaza
     *
     * @param InvoiceRepositoryInterface $subject
     * @param InvoiceInterface $invoice
     * @return InvoiceInterface
     */
    public function afterSave(
        InvoiceRepositoryInterface $subject,
        InvoiceInterface $invoice
    ) {
        try {
            $this->saveExtensionAttribute($invoice);
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }

        return $invoice;
    }

    /**
     * Persist the Spaaza extension attribute
     *
     * @param InvoiceInterface $invoice
     * @return void
     * @throws CouldNotSaveException
     */
    private function saveExtensionAttribute(InvoiceInterface $invoice)
    {
        $extensionAttributes = $invoice->getExtensionAttributes();
        if (!$extensionAttributes) {
            return;
        }

        $spaazaData = $extensionAttributes->getSpaazaData();
        if (!$spaazaData) {
            return;
        }

        try {
            $spaazaData->setInvoiceId($invoice->getEntityId());
            $this->spaazaDataManagement->save($spaazaData);
            $this->helper->debugLog('Saved Spaaza data for invoice', ['invoice' => $invoice->getEntityId()]);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(
                __('Could not save Spaaza data for invoice %1: %2', $invoice->getEntityId(), $e->getMessage()),
                $e
            );
        }
    }
}
