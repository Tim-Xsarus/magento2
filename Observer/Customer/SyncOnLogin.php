<?php

namespace Spaaza\Loyalty\Observer\Customer;

use Magento\Framework\Event\Observer;
use Spaaza\Loyalty\Observer\AbstractObserver;

/**
 * Class SyncOnLogin
 *
 * @event customer_data_object_login
 */
class SyncOnLogin extends AbstractObserver
{
    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Customer\Api\Data\CustomerInterface $customerData */
        $customerData = $observer->getEvent()->getData('customer');
        if (!$this->config->isSyncEnabled()) {
            return;
        }

        $this->customerConnector->updateCustomerFromSpaaza($customerData, true);
    }
}
