<?php

namespace Spaaza\Loyalty\Api;

interface CustomerManagementInterface
{

    /**
     * Authenticate a customer using email and password
     *
     * @param string $email
     * @param string $password
     * @return \Spaaza\Loyalty\Api\Data\AuthenticationResultInterface
     */
    public function authenticate(string $email, string $password);

    /**
     * Update customer data using Spaaza userinfo
     *
     * @param array $userInfo  User data; same as the 'user_info' result in `get-card` Spaaza endpoint
     * @param bool $createIfNotExists  Create a customer if it cannot be identified?
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    public function update(array $userInfo, bool $createIfNotExists = false);

    /**
     * Get Spaaza Data by customer id
     *
     * @param int $customerId
     * @return \Spaaza\Loyalty\Model\Data\UserInfo
     */
    public function getSpaazaUserInfoByCustomerId(int $customerId);

    /**
     * Get Spaaza Data by customer email
     *
     * @param string $customerEmail
     * @return \Spaaza\Loyalty\Model\Data\UserInfo
     */
    public function getSpaazaUserInfoByCustomerEmail(string $customerEmail);
}
