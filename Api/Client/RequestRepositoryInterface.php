<?php

namespace Spaaza\Loyalty\Api\Client;

interface RequestRepositoryInterface
{
    /**
     * Save Request
     *
     * @param \Spaaza\Loyalty\Api\Data\Client\RequestInterface $request
     * @return \Spaaza\Loyalty\Api\Data\Client\RequestInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Spaaza\Loyalty\Api\Data\Client\RequestInterface $request
    );

    /**
     * Retrieve Request
     *
     * @param string $requestId
     * @return \Spaaza\Loyalty\Api\Data\Client\RequestInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($requestId);

    /**
     * Retrieve Request matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Spaaza\Loyalty\Api\Data\Client\RequestSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Request
     *
     * @param \Spaaza\Loyalty\Api\Data\Client\RequestInterface $request
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Spaaza\Loyalty\Api\Data\Client\RequestInterface $request
    );

    /**
     * Delete Request by ID
     *
     * @param string $requestId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($requestId);
}
