<?php

namespace Spaaza\Loyalty\Api\Data\Order;

interface SpaazaDataInterface
{
    const KEY_ORDER_ID = 'order_id';
    const KEY_BASKET_ID = 'basket_id';
    const KEY_MEMBER_NUMBER = 'member_number';
    const KEY_USER_ID = 'user_id';
    const KEY_VOUCHERS = 'vouchers';
    const KEY_VOUCHER_DISTRIBUTION = 'voucher_distribution';
    const KEY_VOUCHER_AMOUNT = 'voucher_amount';
    const KEY_BASE_VOUCHER_AMOUNT = 'base_voucher_amount';
    const KEY_VOUCHER_AMOUNT_INVOICED = 'voucher_amount_invoiced';
    const KEY_BASE_VOUCHER_AMOUNT_INVOICED = 'base_voucher_amount_invoiced';
    const KEY_VOUCHER_AMOUNT_REFUNDED = 'voucher_amount_refunded';
    const KEY_BASE_VOUCHER_AMOUNT_REFUNDED = 'base_voucher_amount_refunded';

    /**
     * Get Order Id
     *
     * @return int
     */
    public function getOrderId();

    /**
     * Set Order Id
     *
     * @param int $orderId
     * @return $this
     */
    public function setOrderId($orderId);

    /**
     * Get Basket Id
     *
     * @return int|null
     */
    public function getBasketId();

    /**
     * Set Basket Id
     *
     * @param int|null $basketId
     * @return $this
     */
    public function setBasketId(?int $basketId);

    /**
     * Get Member Number
     *
     * @return string|null
     */
    public function getMemberNumber();

    /**
     * Set Member Number
     *
     * @param string|null $memberNumber
     * @return $this
     */
    public function setMemberNumber($memberNumber);

    /**
     * Get User Id
     *
     * @return int|null
     */
    public function getUserId();

    /**
     * Set User Id
     *
     * @param int|null $userId
     * @return $this
     */
    public function setUserId(?int $userId);

    /**
     * Get Vouchers
     *
     * @return \Spaaza\Loyalty\Api\Data\VoucherInfoInterface[]
     */
    public function getVouchers();

    /**
     * Get Voucher Distribution
     *
     * @return array|null
     */
    public function getVoucherDistribution();

    /**
     * Set Voucher Distribution
     *
     * @param array|null $voucherDistribution
     * @return $this
     */
    public function setVoucherDistribution(?array $voucherDistribution);

    /**
     * Set Vouchers
     *
     * @param \Spaaza\Loyalty\Api\Data\VoucherInfoInterface[] $vouchers
     * @return $this
     */
    public function setVouchers($vouchers);

    /**
     * Get Voucher Amount
     *
     * @return float
     */
    public function getVoucherAmount();

    /**
     * Set Voucher Amount
     *
     * @param float $voucherAmount
     * @return $this
     */
    public function setVoucherAmount($voucherAmount);

    /**
     * Get Base Voucher Amount
     *
     * @return float
     */
    public function getBaseVoucherAmount();

    /**
     * Set Base Voucher Amount
     *
     * @param float $baseVoucherAmount
     * @return $this
     */
    public function setBaseVoucherAmount($baseVoucherAmount);

    /**
     * Get Voucher Amount Invoiced
     *
     * @return float
     */
    public function getVoucherAmountInvoiced();

    /**
     * Set Voucher Amount Invoiced
     *
     * @param float $voucherAmountInvoiced
     * @return $this
     */
    public function setVoucherAmountInvoiced($voucherAmountInvoiced);

    /**
     * Get Base Voucher Amount Invoiced
     *
     * @return float
     */
    public function getBaseVoucherAmountInvoiced();

    /**
     * Set Base Voucher Amount Invoiced
     *
     * @param float $baseVoucherAmountInvoiced
     * @return $this
     */
    public function setBaseVoucherAmountInvoiced($baseVoucherAmountInvoiced);

    /**
     * Get Voucher Amount Refunded
     *
     * @return float
     */
    public function getVoucherAmountRefunded();

    /**
     * Set Voucher Amount Refunded
     *
     * @param float $voucherAmountRefunded
     * @return $this
     */
    public function setVoucherAmountRefunded($voucherAmountRefunded);

    /**
     * Get Base Voucher Amount Refunded
     *
     * @return float
     */
    public function getBaseVoucherAmountRefunded();

    /**
     * Set Base Voucher Amount Refunded
     *
     * @param float $baseVoucherAmountRefunded
     * @return $this
     */
    public function setBaseVoucherAmountRefunded($baseVoucherAmountRefunded);
}
