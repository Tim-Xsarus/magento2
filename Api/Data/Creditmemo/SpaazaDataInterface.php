<?php

namespace Spaaza\Loyalty\Api\Data\Creditmemo;

interface SpaazaDataInterface
{
    const KEY_CREDITMEMO_ID = 'creditmemo_id';
    const KEY_VOUCHER_AMOUNT = 'voucher_amount';
    const KEY_BASE_VOUCHER_AMOUNT = 'base_voucher_amount';
    const KEY_BASKET_ID = 'basket_id';

    /**
     * Get Creditmemo Id
     *
     * @return int|null
     */
    public function getCreditmemoId(): ?int;

    /**
     * Set Creditmemo Id
     *
     * @param int $creditmemoId
     * @return $this
     */
    public function setCreditmemoId(int $creditmemoId);

    /**
     * Get Voucher Amount
     *
     * @return float|null
     */
    public function getVoucherAmount(): ?float;

    /**
     * Set Voucher Amount
     *
     * @param float $voucherAmount
     * @return $this
     */
    public function setVoucherAmount(float $voucherAmount);

    /**
     * Get Base Voucher Amount
     *
     * @return float|null
     */
    public function getBaseVoucherAmount(): ?float;

    /**
     * Set Base Voucher Amount
     *
     * @param float $baseVoucherAmount
     * @return $this
     */
    public function setBaseVoucherAmount(float $baseVoucherAmount);

    /**
     * Get Basket Id
     *
     * @return int|null
     */
    public function getBasketId(): ?int;

    /**
     * Set Basket Id
     *
     * @param int|null $basketId
     * @return $this
     */
    public function setBasketId(?int $basketId);
}
