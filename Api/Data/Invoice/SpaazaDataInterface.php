<?php

namespace Spaaza\Loyalty\Api\Data\Invoice;

interface SpaazaDataInterface
{
    const KEY_INVOICE_ID = 'invoice_id';
    const KEY_VOUCHER_AMOUNT = 'voucher_amount';
    const KEY_BASE_VOUCHER_AMOUNT = 'base_voucher_amount';

    /**
     * Get Invoice Id
     *
     * @return int
     */
    public function getInvoiceId();

    /**
     * Set Invoice Id
     *
     * @param int|null $invoiceId
     * @return $this
     */
    public function setInvoiceId(?int $invoiceId);

    /**
     * Get Voucher Amount
     *
     * @return float
     */
    public function getVoucherAmount();

    /**
     * Set Voucher Amount
     *
     * @param float $voucherAmount
     * @return $this
     */
    public function setVoucherAmount($voucherAmount);

    /**
     * Get Base Voucher Amount
     *
     * @return float
     */
    public function getBaseVoucherAmount();

    /**
     * Set Base Voucher Amount
     *
     * @param float $baseVoucherAmount
     * @return $this
     */
    public function setBaseVoucherAmount($baseVoucherAmount);
}
