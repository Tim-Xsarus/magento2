<?php

namespace Spaaza\Loyalty\Api\Data\Client;

use Spaaza\Client as SpaazaClient;

interface RequestInterface
{
    const KEY_REQUEST_ID = 'request_id';
    const KEY_ENTITY_TYPE = 'entity_type';
    const KEY_ENTITY_ID = 'entity_id';
    const KEY_PATH = 'path';
    const KEY_METHOD = 'method';
    const KEY_PAYLOAD = 'payload';
    const KEY_OPTIONS = 'options';
    const KEY_IS_SYNCHRONOUS = 'is_synchronous';
    const KEY_STATUS = 'status';
    const KEY_RESPONSE = 'response';
    const KEY_CALLBACK = 'callback';
    const KEY_MESSAGE = 'message';
    const KEY_EXECUTION_TIME = 'execution_time';
    const KEY_ATTEMPTS = 'attempts';
    const KEY_CREATED_AT = 'created_at';
    const KEY_SENT_AT = 'sent_at';

    const STATUS_PENDING = 'pending';
    const STATUS_PROCESSING = 'processing';
    const STATUS_COMPLETE = 'complete';
    const STATUS_ERROR = 'error';

    const METHOD_POST = SpaazaClient::METHOD_POST;
    const METHOD_GET = SpaazaClient::METHOD_GET;
    const METHOD_POST_JSON = SpaazaClient::METHOD_POST_JSON;
    const METHOD_PUT = SpaazaClient::METHOD_PUT;
    const METHOD_DELETE = SpaazaClient::METHOD_DELETE;
    const METHOD_DELETE_JSON = SpaazaClient::METHOD_DELETE_JSON;

    /**
     * Get Payload
     *
     * @return mixed
     */
    public function getPayload();

    /**
     * Set Payload
     *
     * This data gets json_encode()d before being sent
     *
     * @param mixed $payload
     * @return $this
     */
    public function setPayload($payload);

    /**
     * Get Options
     *
     * @return array
     */
    public function getOptions(): array;

    /**
     * Set Options
     *
     * @param array $options Associative array with options
     * @return $this
     */
    public function setOptions($options);

    /**
     * Get Request Id
     *
     * @return int
     */
    public function getRequestId();

    /**
     * Set Request Id
     *
     * @param int $requestId
     * @return $this
     */
    public function setRequestId($requestId);

    /**
     * Get Entity Type
     *
     * @return string|null
     */
    public function getEntityType();

    /**
     * Set Entity Type
     *
     * @param string|null $entityType
     * @return $this
     */
    public function setEntityType($entityType);

    /**
     * Get Entity Id
     *
     * @return int|null
     */
    public function getEntityId();

    /**
     * Set Entity Id
     *
     * @param int|null $entityId
     * @return $this
     */
    public function setEntityId($entityId);

    /**
     * Get Path
     *
     * @return string
     */
    public function getPath();

    /**
     * Set Path
     *
     * @param string $path
     * @return $this
     */
    public function setPath($path);

    /**
     * Get Method
     *
     * @return string
     */
    public function getMethod();

    /**
     * Set Method
     *
     * @param string $method
     * @return $this
     */
    public function setMethod($method);

    /**
     * Get Is Synchronous
     *
     * @return bool
     */
    public function getIsSynchronous();

    /**
     * Set Is Synchronous
     *
     * @param bool $isSynchronous
     * @return $this
     */
    public function setIsSynchronous($isSynchronous);

    /**
     * Get Status
     *
     * @return string
     */
    public function getStatus();

    /**
     * Set Status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * Get Response
     *
     * @return string|null
     */
    public function getResponse();

    /**
     * Set Response
     *
     * @param string|null $response
     * @return $this
     */
    public function setResponse($response);

    /**
     * Get Callback
     *
     * @return string|null
     */
    public function getCallback();

    /**
     * Set Callback
     *
     * @param string|null $callback
     * @return $this
     */
    public function setCallback($callback);

    /**
     * Get Message
     *
     * @return string|null
     */
    public function getMessage();

    /**
     * Set Message
     *
     * @param string|null $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * Get Execution Time
     *
     * @return int|null
     */
    public function getExecutionTime();

    /**
     * Set Execution Time
     *
     * @param int|null $executionTime
     * @return $this
     */
    public function setExecutionTime(?int $executionTime);

    /**
     * Get Attempts
     *
     * @return int
     */
    public function getAttempts();

    /**
     * Set Attempts
     *
     * @param int $attempts
     * @return $this
     */
    public function setAttempts(int $attempts);

    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set Created At
     *
     * @param string|null $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Get Sent At
     *
     * @return string|null
     */
    public function getSentAt();

    /**
     * Set Sent At
     *
     * @param string|null $sentAt
     * @return $this
     */
    public function setSentAt($sentAt);
}
