<?php

namespace Spaaza\Loyalty\Api\Data;

interface AuthenticationResultInterface
{
    /**
     * Get authenticated flag
     *
     * @return bool
     */
    public function getAuthenticated();

    /**
     * Set authenticated flag
     *
     * @param bool $authenticated
     * @return $this
     */
    public function setAuthenticated($authenticated);

    /**
     * Get customer id
     *
     * @return int
     */
    public function getCustomerId();

    /**
     * Set customer id
     *
     * @param int|null $customerId
     * @return $this
     */
    public function setCustomerId(?int $customerId);

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage();

    /**
     * Set message
     *
     * @param string|null $message
     * @return $this
     */
    public function setMessage(?string $message);
}
