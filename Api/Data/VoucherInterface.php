<?php

namespace Spaaza\Loyalty\Api\Data;

interface VoucherInterface
{
    const STATUS_CLAIMED = 'claimed';
    const STATUS_GENERATED = 'generated';
    const STATUS_REDEEMED = 'redeemed';

    const VOUCHER_TYPE_BASKET = 'basket';
    const VOUCHER_TYPE_HONOUR = 'honour';

    const KEY_VOUCHER_KEY = 'voucher_key';
    const KEY_VOUCHER_ID = 'voucher_id';
    const KEY_VOUCHER_STATUS = 'voucher_status';
    const KEY_VOUCHER_LOCKED = 'voucher_locked';
    const KEY_VOUCHER_TYPE = 'voucher_type';
    const KEY_CAMPAIGN_ID = 'campaign_id';
    const KEY_CAMPAIGN_TYPE = 'campaign_type';
    const KEY_CAMPAIGN_TITLE = 'campaign_title';
    const KEY_VOUCHER_EXPIRY_DATETIME_UTC = 'voucher_expiry_datetime_utc';
    const KEY_VOUCHER_EXPIRY_SECONDS_REMAINING = 'voucher_expiry_seconds_remaining';
    const KEY_VOUCHER_CURRENCY_ID = 'voucher_currency_id';
    const KEY_VOUCHER_CURRENCY_SYMBOL = 'voucher_currency_symbol';
    const KEY_VOUCHER_AMOUNT_ORIGINAL = 'voucher_amount_original';
    const KEY_VOUCHER_AMOUNT_REDEEMED = 'voucher_amount_redeemed';
    const KEY_VOUCHER_AMOUNT = 'voucher_amount';
    const KEY_VOUCHER_TEXT = 'voucher_text';
    const KEY_VOUCHER_LOCKING_CODE = 'voucher_locking_code';
    const KEY_VOUCHER_BASKET_OWNER_CODE_EXCLUSIVE = 'voucher_basket_owner_code_exclusive';
    const KEY_VOUCHER_HONOUR_CODE = 'voucher_honour_code';
    const KEY_VOUCHER_CREATED_DATETIME = 'voucher_created_datetime';
    const KEY_CAMPAIGN_IMAGE_FILENAME = 'campaign_image_filename';
    const KEY_CAMPAIGN_IMAGE_URL = 'campaign_image_url';
    const KEY_CAMPAIGN_IMAGE_LINK = 'campaign_image_link';
    const KEY_CAMPAIGN_IMAGE_DIMENSION_X = 'campaign_image_dimension_x';
    const KEY_CAMPAIGN_IMAGE_DIMENSION_Y = 'campaign_image_dimension_y';
    const KEY_VOUCHER_REDEEMED_DATETIME = 'voucher_redeemed_datetime';
    const KEY_PARENT_VOUCHER = 'parent_voucher';
    const KEY_VOUCHER_TITLE = 'voucher_title';
    const KEY_VOUCHER_DESCRIPTION = 'voucher_description';
    const KEY_VOUCHER_NOTES = 'voucher_notes';
    const KEY_VOUCHER_IMAGE_URL = 'voucher_image_url';
    const KEY_VOUCHER_DISCOUNT_RATIO = 'voucher_discount_ratio';

    /**
     * Get Voucher Key
     *
     * @return string|null
     */
    public function getVoucherKey(): ?string;

    /**
     * Set Voucher Key
     *
     * @param string $voucherKey
     * @return $this
     */
    public function setVoucherKey(string $voucherKey);

    /**
     * Get Voucher Id
     *
     * @return int|null
     */
    public function getVoucherId(): ?int;

    /**
     * Set Voucher Id
     *
     * @param int|null $voucherId
     * @return $this
     */
    public function setVoucherId(?int $voucherId);

    /**
     * Get Voucher Status
     *
     * @return string|null
     */
    public function getVoucherStatus(): ?string;

    /**
     * Set Voucher Status
     *
     * @param string $voucherStatus
     * @return $this
     */
    public function setVoucherStatus(string $voucherStatus);

    /**
     * Get Voucher Type
     *
     * @return string|null
     */
    public function getVoucherType(): ?string;

    /**
     * Set Voucher Type
     *
     * @param string $voucherType
     * @return $this
     */
    public function setVoucherType(string $voucherType);

    /**
     * Get Voucher Locked
     *
     * @return bool
     */
    public function getVoucherLocked(): bool;

    /**
     * Set Voucher Locked
     *
     * @param bool $voucherLocked
     * @return $this
     */
    public function setVoucherLocked(bool $voucherLocked);

    /**
     * Get Campaign Id
     *
     * @return int
     */
    public function getCampaignId(): ?int;

    /**
     * Set Campaign Id
     *
     * @param int $campaignId
     * @return $this
     */
    public function setCampaignId(int $campaignId);

    /**
     * Get Campaign Type
     *
     * @return string|null
     */
    public function getCampaignType(): ?string;

    /**
     * Set Campaign Type
     *
     * @param string|null $campaignType
     * @return $this
     */
    public function setCampaignType(?string $campaignType);

    /**
     * Get Campaign Title
     *
     * @return string|null
     */
    public function getCampaignTitle(): ?string;

    /**
     * Set Campaign Title
     *
     * @param string|null $campaignTitle
     * @return $this
     */
    public function setCampaignTitle(?string $campaignTitle);

    /**
     * Get Voucher Expiry Datetime Utc
     *
     * @return string|null
     */
    public function getVoucherExpiryDatetimeUtc(): ?string;

    /**
     * Set Voucher Expiry Datetime Utc
     *
     * @param string|null $voucherExpiryDatetimeUtc
     * @return $this
     */
    public function setVoucherExpiryDatetimeUtc(?string $voucherExpiryDatetimeUtc);

    /**
     * Get Voucher Expiry Seconds Remaining
     *
     * @return int|null
     */
    public function getVoucherExpirySecondsRemaining();

    /**
     * Set Voucher Expiry Seconds Remaining
     *
     * @param int|null $voucherExpirySecondsRemaining
     * @return $this
     */
    public function setVoucherExpirySecondsRemaining(?int $voucherExpirySecondsRemaining);

    /**
     * Get Voucher Currency Id
     *
     * @return int|null
     */
    public function getVoucherCurrencyId(): ?int;

    /**
     * Set Voucher Currency Id
     *
     * @param int $voucherCurrencyId
     * @return $this
     */
    public function setVoucherCurrencyId(int $voucherCurrencyId);

    /**
     * Get Voucher Currency Symbol
     *
     * @return string|null
     */
    public function getVoucherCurrencySymbol(): ?string;

    /**
     * Set Voucher Currency Symbol
     *
     * @param string $voucherCurrencySymbol
     * @return $this
     */
    public function setVoucherCurrencySymbol(string $voucherCurrencySymbol);

    /**
     * Get Voucher Amount Original
     *
     * @return float|null
     */
    public function getVoucherAmountOriginal(): ?float;

    /**
     * Set Voucher Amount Original
     *
     * @param float|null $voucherAmountOriginal
     * @return $this
     */
    public function setVoucherAmountOriginal(?float $voucherAmountOriginal);

    /**
     * Get Voucher Amount Redeemed
     *
     * @return float
     */
    public function getVoucherAmountRedeemed(): float;

    /**
     * Set Voucher Amount Redeemed
     *
     * @param float $voucherAmountRedeemed
     * @return $this
     */
    public function setVoucherAmountRedeemed(float $voucherAmountRedeemed);

    /**
     * Get Voucher Amount
     *
     * @return float|null
     */
    public function getVoucherAmount(): ?float;

    /**
     * Set Voucher Amount
     *
     * @param float $voucherAmount
     * @return $this
     */
    public function setVoucherAmount(float $voucherAmount);

    /**
     * Get Voucher Text
     *
     * @return string|null
     */
    public function getVoucherText(): ?string;

    /**
     * Set Voucher Text
     *
     * @param string|null $voucherText
     * @return $this
     */
    public function setVoucherText(?string $voucherText);

    /**
     * Get Voucher Locking Code
     *
     * @return string|null
     */
    public function getVoucherLockingCode(): ?string;

    /**
     * Set Voucher Locking Code
     *
     * @param string|null $voucherLockingCode
     * @return $this
     */
    public function setVoucherLockingCode(?string $voucherLockingCode);

    /**
     * Get Voucher Basket Owner Code Exclusive
     *
     * @return string|null
     */
    public function getVoucherBasketOwnerCodeExclusive(): ?string;

    /**
     * Set Voucher Basket Owner Code Exclusive
     *
     * @param string|null $voucherBasketOwnerCodeExclusive
     * @return $this
     */
    public function setVoucherBasketOwnerCodeExclusive(?string $voucherBasketOwnerCodeExclusive);

    /**
     * Get Voucher Honour Code
     *
     * @return string|null
     */
    public function getVoucherHonourCode(): ?string;

    /**
     * Set Voucher Honour Code
     *
     * @param string|null $voucherHonourCode
     * @return $this
     */
    public function setVoucherHonourCode(?string $voucherHonourCode);

    /**
     * Get Voucher Created Datetime
     *
     * @return string|null
     */
    public function getVoucherCreatedDatetime(): ?string;

    /**
     * Set Voucher Created Datetime
     *
     * @param string|null $voucherCreatedDatetime
     * @return $this
     */
    public function setVoucherCreatedDatetime(?string $voucherCreatedDatetime);

    /**
     * Get Campaign Image Filename
     *
     * @return string|null
     */
    public function getCampaignImageFilename(): ?string;

    /**
     * Set Campaign Image Filename
     *
     * @param string|null $campaignImageFilename
     * @return $this
     */
    public function setCampaignImageFilename(?string $campaignImageFilename);

    /**
     * Get Campaign Image Url
     *
     * @return string|null
     */
    public function getCampaignImageUrl(): ?string;

    /**
     * Set Campaign Image Url
     *
     * @param string|null $campaignImageUrl
     * @return $this
     */
    public function setCampaignImageUrl(?string $campaignImageUrl);

    /**
     * Get Campaign Image Link
     *
     * @return string|null
     */
    public function getCampaignImageLink(): ?string;

    /**
     * Set Campaign Image Link
     *
     * @param string|null $campaignImageLink
     * @return $this
     */
    public function setCampaignImageLink(?string $campaignImageLink);

    /**
     * Get Campaign Image Dimension X
     *
     * @return int|null
     */
    public function getCampaignImageDimensionX(): ?int;

    /**
     * Set Campaign Image Dimension X
     *
     * @param int|null $campaignImageDimensionX
     * @return $this
     */
    public function setCampaignImageDimensionX(?int $campaignImageDimensionX);

    /**
     * Get Campaign Image Dimension Y
     *
     * @return int|null
     */
    public function getCampaignImageDimensionY(): ?int;

    /**
     * Set Campaign Image Dimension Y
     *
     * @param int|null $campaignImageDimensionY
     * @return $this
     */
    public function setCampaignImageDimensionY(?int $campaignImageDimensionY);

    /**
     * Get Voucher Redeemed Datetime
     *
     * @return string|null
     */
    public function getVoucherRedeemedDatetime(): ?string;

    /**
     * Set Voucher Redeemed Datetime
     *
     * @param string|null $voucherRedeemedDatetime
     * @return $this
     */
    public function setVoucherRedeemedDatetime(?string $voucherRedeemedDatetime);

    /**
     * Get Parent Voucher
     *
     * @return string|null
     */
    public function getParentVoucher(): ?string;

    /**
     * Set Parent Voucher
     *
     * @param string|null $parentVoucher
     * @return $this
     */
    public function setParentVoucher(?string $parentVoucher);

    /**
     * Get Voucher Title
     *
     * @return string|null
     */
    public function getVoucherTitle(): ?string;

    /**
     * Set Voucher Title
     *
     * @param string|null $voucherTitle
     * @return $this
     */
    public function setVoucherTitle(?string $voucherTitle);

    /**
     * Get Voucher Description
     *
     * @return string|null
     */
    public function getVoucherDescription(): ?string;

    /**
     * Set Voucher Description
     *
     * @param string|null $voucherDescription
     * @return $this
     */
    public function setVoucherDescription(?string $voucherDescription);

    /**
     * Get Voucher Notes
     *
     * @return string|null
     */
    public function getVoucherNotes(): ?string;

    /**
     * Set Voucher Notes
     *
     * @param string|null $voucherNotes
     * @return $this
     */
    public function setVoucherNotes(?string $voucherNotes);

    /**
     * Get Voucher Image Url
     *
     * @return string|null
     */
    public function getVoucherImageUrl(): ?string;

    /**
     * Set Voucher Image Url
     *
     * @param string|null $voucherImageUrl
     * @return $this
     */
    public function setVoucherImageUrl(?string $voucherImageUrl);

    /**
     * Get Voucher Discount Ratio
     *
     * @return float|null
     */
    public function getVoucherDiscountRatio(): ?float;

    /**
     * Set Voucher Discount Ratio
     *
     * @param float|null $voucherDiscountRatio
     * @return $this
     */
    public function setVoucherDiscountRatio(?float $voucherDiscountRatio);
}
